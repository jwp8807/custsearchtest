﻿//Check the APRO checkbox by default
$(document).ready(function () {
    PrepareCheckbox();
});
function PrepareCheckbox() {
    document.getElementById("checkBoxAproSearch").checked = true;
}


$(document).on('change', '#checkBoxAproSearch', function () {
    if (this.checked) {
        $('#checkBoxCustomerSearch').prop('checked', false);
    }
});

$(document).on('change', '#checkBoxCustomerSearch', function () {
    if (this.checked) {
        $('#checkBoxAproSearch').prop('checked', false);
    }
});