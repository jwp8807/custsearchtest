﻿$("body").on("click", "#resultsTable .GetOrderHistoryButton", function () {
    var row = $(this).closest("tr");
    var orderHistoryRowIndex = row.attr('class');
    $("#" + orderHistoryRowIndex + ".loaderOrderHistory").show();
    var currentGetOrderHistoryButton = $(this);
    currentGetOrderHistoryButton.text("Getting order history...")
    currentGetOrderHistoryButton.attr("disabled", true);
    var startDate = $("#datePickerStartDate").text();
    var endDate = $("#datePickerEndDate").text();
    var bpcsCustomerNum = this.id;

    $.ajax({
        type: 'POST',
        url: '/Customer/GetOrderHistory',
        data: {
            'bpcsCustomerNum': bpcsCustomerNum,
            'startDate': '2015-12-13',
            'endDate': '2018-12-13'
        },
        success: function (result) {
            //$("#" + orderHistoryRowIndex + ".orderHistoryRow").show();
            //alert("#orderHistoryPartialView." + orderHistoryRowIndex + "");
            $("#orderHistoryPartialView." + orderHistoryRowIndex + "").html(result);
            $("#" + orderHistoryRowIndex + ".loaderOrderHistory").hide();
            currentGetOrderHistoryButton.hide();
        }
    });
});