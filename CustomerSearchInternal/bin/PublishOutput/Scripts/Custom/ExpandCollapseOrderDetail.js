﻿$("body").on("click", "#orderTable .btnExpandCollapseOrderDetail", function () {
    var row = $(this).closest("tr");
    var orderDetailRowIndex = row.attr('class');
    var invoiceNum = row.find("#invoiceNum").text();
    var bpcsCustomerNum = this.id;
    var isDetailsRowExpanded = row.find("#hiddenIsDetailsExpanded").val();

    if (isDetailsRowExpanded != true) {
        $.ajax({
            type: 'POST',
            url: '/Customer/GetOrderDetail',
            data: {
                'bpcsCustomerNum': bpcsCustomerNum,
                'invoiceNum': invoiceNum,
                'startDate': '2015-12-13',
                'endDate': '2018-12-13'
            },
            success: function (result) {
                //Set the hidden is details row expanded value
                row.find("#hiddenIsDetailsExpanded").val(true);

                //Set the order details partial view html and then show it
                $("#orderDetailPartialView." + orderDetailRowIndex + "").html(result);
                $("#orderHistoryDetailActualRow." + orderDetailRowIndex + "").show();
            }
        });
    }
    else {
        //If order detail row already expanded then collapse the row
        $("#orderHistoryDetailActualRow." + orderDetailRowIndex + "").hide();

        //Set the hidden is details row expanded value
        row.find("#hiddenIsDetailsExpanded").val(false);
    }
});