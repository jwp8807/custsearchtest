﻿namespace CustomerSearchInternal.Controllers
{
    using ClosedXML.Excel;
    #region NAMESPACES
    using CustomerSearchInternal.BusinessLayer;
    using CustomerSearchInternal.Common;
    using CustomerSearchInternal.Enum;
    using CustomerSearchInternal.Logging;
    using CustomerSearchInternal.Models;
    using CustomerSearchInternal.Models.Request;
    using CustomerSearchInternal.Session;
    using CustomerSearchInternal.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Services;
    #endregion

    public class CustomerController : Controller
    {
        #region FIELDS
        CustomerSearchBusinessManager _customerSearchBusinessManager;
        List<SearchCriteria> _searchCriteria;
        #endregion

        #region CONSTRUCTORS
        public CustomerController()
        {
            if (_customerSearchBusinessManager == null)
            {
                _customerSearchBusinessManager = new CustomerSearchBusinessManager();
            }
        }
        #endregion

        #region PUBLIC METHODS
        [HttpGet]
        public ActionResult Search(CustomerSearchViewModel viewModel)
        {
            try
            {
                if(viewModel == null)
                {
                    viewModel = new CustomerSearchViewModel
                    {
                        States = GetStates()
                    };
                }
                else
                {
                    viewModel.States = GetStates();
                }

                SessionData.Put("states", viewModel.States);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
                return View("UnexpectedError");
            }
        }

        public ActionResult SubmitSearch(CustomerSearchViewModel viewModel)
        {
            ResultsViewMode viewMode = ResultsViewMode.Stretch;
            CustomerSearchViewModel vm = new CustomerSearchViewModel();
            CustomerSearchResult result = new CustomerSearchResult();
            //Validate Search Criteria
            if (ValidateSearchCriteria(viewModel.SearchCriteria))
            {
                result = _customerSearchBusinessManager.GetCustomerMatches(viewModel.SearchCriteria);
                SessionData.Put("customerSearchResult", result);

                string resultMessage = string.Empty;
                if (!string.IsNullOrWhiteSpace(result.Result.Code) && result.Result.Code.Equals("005"))
                {
                    resultMessage = !string.IsNullOrWhiteSpace(result.Result.Message) ? string.Format("{0} - {1}", result.Result.Code, result.Result.Message) : result.Result.Code;

                    resultMessage = string.Concat("Web service response message : ", resultMessage);

                    //Return this result to message div in view
                }

                if (result.Customers.Count == 0)
                {
                    string defaultMessage = "No results found for search criteria entered.";

                    if (!string.IsNullOrWhiteSpace(result.Result.Code))
                    {
                        resultMessage = !string.IsNullOrWhiteSpace(result.Result.Message) ? string.Format("{0} - {1}", result.Result.Code, result.Result.Message) : result.Result.Code;

                        resultMessage = string.Concat("Web service response message : ", resultMessage);
                        //Return this result to message div in view
                    }
                }
                else
                {
                    viewModel.CustomerSearchResult = result;
                }
            }

            viewModel.States = SessionData.Get<List<StateCode>>("states");
            viewModel.ResultsViewMode = viewMode;

            return View("Search", viewModel);
        }

        public PartialViewResult DisplayImportSearchResults(CustomerSearchViewModel viewModel)
        {
            foreach(var item in viewModel.ImportResult.Results)
            {
                viewModel.CustomerSearchResult.Customers.AddRange(item.Customers);
            }

            return PartialView("~/Views/Customer/PartialViews/_ImportResults.cshtml", viewModel);
        }

        public PartialViewResult GetOrderHistory(string bpcsCustomerNum, DateTime startDate, DateTime endDate)
        {
            startDate = DateTime.Now.AddYears(-3);
            endDate = DateTime.Now;
            CustomerSearchViewModel viewModel = new CustomerSearchViewModel();
            try
            {
                if (!string.IsNullOrEmpty(bpcsCustomerNum) && ValidateStartEndDate(startDate, endDate))
                {
                    viewModel.OrderHistory = new List<OrderHistory>(_customerSearchBusinessManager.GetOrderHistory(bpcsCustomerNum, startDate, endDate));
                }

                //Store in Session for Order Detail Functionality
                SessionData.Put("OrderHistory", viewModel.OrderHistory);
            }
            catch(Exception ex)
            {
                Logger.Error("Error occurred at GetOrderHistory(). " + ex.ToString());
            }

            return PartialView("~/Views/Customer/PartialViews/_OrderHistory.cshtml", viewModel);
        }

        public PartialViewResult GetOrderDetail(string bpcsCustomerNum, string invoiceNum, DateTime startDate, DateTime endDate)
        {
            startDate = DateTime.Now.AddYears(-3);
            endDate = DateTime.Now;
            CustomerSearchViewModel viewModel = new CustomerSearchViewModel();
            try
            {
                if (!string.IsNullOrEmpty(bpcsCustomerNum) && !string.IsNullOrEmpty(invoiceNum) && ValidateStartEndDate(startDate, endDate))
                {
                    //Get the Order History from session
                    //viewModel.OrderHistory = SessionData.Get<List<OrderHistory>>("OrderHistory");

                    //Get the Order History object from the order history list by the invoice number
                    //OrderHistory selectedOrderHistory = viewModel.OrderHistory.Find(item => item.InvoiceNumber == "invoiceNum");

                    //Get the order detail for selected order history item
                    List<OrderDetail> orderDetail = new List<OrderDetail>(_customerSearchBusinessManager.GetOrderDetail(bpcsCustomerNum, invoiceNum, startDate, endDate));
                    viewModel.OrderDetail = orderDetail;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error occurred at GetOrderDetail(). " + ex.ToString());
            }

            return PartialView("~/Views/Customer/PartialViews/_OrderDetail.cshtml", viewModel);
        }

        public JsonResult SubmitImportSpreadsheet(CustomerSearchViewModel viewModel = null, string emailAddress = null, string dropDownIncludeHistory = null, string dropDownExcludeDist = null, string distributorsToExclude = null)
        {
            CustomerSearchViewModel newViewModel = new CustomerSearchViewModel();
            string resultMessage = string.Empty;

            //Get the isCustomerSearch & isAproSearch values from session
            bool isCustomerSearch = SessionData.Get<bool>("isCustomerSearch");
            bool isAproSearch = SessionData.Get<bool>("isAproSearch");

            //If these values aren't null or empty then it needs to be processed through the batch process
            if (!string.IsNullOrEmpty(emailAddress) && !string.IsNullOrEmpty(dropDownIncludeHistory) && !string.IsNullOrEmpty(dropDownExcludeDist))
            {
                viewModel = new CustomerSearchViewModel
                {
                    ImportRequest = new ImportRequest
                    {
                        EmailAddress = emailAddress,
                        DistributorsToExlude = distributorsToExclude,
                        ExcludeDistributor = dropDownExcludeDist.Substring(0, 1),
                        ExcludeHistory = dropDownIncludeHistory.ToUpper().Trim() == "YES" ? "N" : "Y"
                    },
                    SearchCriteria = new SearchCriteria
                    {
                        IsCustomerSearch = false,
                        IsAproSearch = true
                    }
                };
            }
            else
            {
                viewModel.SearchCriteria.IsCustomerSearch = isCustomerSearch;
                viewModel.SearchCriteria.IsAproSearch = isAproSearch;
            }

            //Get the import file object from session
            viewModel.ImportRequest.File = SessionData.Get<HttpPostedFileBase>("importFile");
            if(viewModel.ImportRequest.File != null)
            {
                string fileName = viewModel.ImportRequest.File.FileName;
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    //// Get list of orders from excel file.
                    newViewModel = ReadExcelFile(fileName, viewModel.SearchCriteria.IsCustomerSearch, viewModel.ImportRequest);

                    bool IsImportSuccessful = newViewModel.ImportResult.IsImportSuccessful;
                    bool IsAproSearch = viewModel.SearchCriteria.IsAproSearch;
                    int WorksheetRowCount = SessionData.Get<int>("importSpreadsheetRowCount");
                    int SearchCountLimit = Convert.ToInt32(ConfigurationManager.AppSettings["SearchCountLimit"]);

                    //Check if read file method was successful
                    if (newViewModel.ImportResult.IsImportSuccessful && viewModel.SearchCriteria.IsCustomerSearch ||
                        newViewModel.ImportResult.IsImportSuccessful && viewModel.SearchCriteria.IsAproSearch && SessionData.Get<int>("importSpreadsheetRowCount") < Convert.ToInt32(ConfigurationManager.AppSettings["SearchCountLimit"]))
                    {
                        List<string> lstBPCSCustNo = new List<string>();
                        foreach (SearchCriteria criteria in newViewModel.SearchCriterias)
                        {
                            SearchResult result = new SearchResult();
                            result.Criteria = criteria;
                            criteria.IsCustomerSearch = isCustomerSearch;
                            criteria.IsAproSearch = isAproSearch;

                            if (ValidateSearchCriteriaForImport(criteria))
                            {
                                //Get the customer search results
                                //CustomerSearchViewModel viewModel = new CustomerSearchViewModel() { SearchCriteria = criteria, IsCustomerSearchMode = viewModel.IsCustomerSearchMode };
                                CustomerSearchResult customerSearchResult = _customerSearchBusinessManager.GetCustomerMatches(criteria);

                                //Add standardized results
                                result.Criteria.StandardizedAddress = customerSearchResult.StandardizedAddressLine;
                                result.Criteria.StandardizedZip = customerSearchResult.StandardizedZip;
                                result.Criteria.PerMailError = customerSearchResult.PerMailError;
                                result.Customers = customerSearchResult.Customers;

                                if (customerSearchResult.Customers.Count > 0)
                                {
                                    foreach (var customer in customerSearchResult.Customers)
                                    {
                                        lstBPCSCustNo.Add(customer.BPCSCustomerNumber);
                                    }
                                }
                            }
                            else
                            {
                                Logger.Error("ERROR");
                            }

                            //Seperate match or no match results in order to display them separately
                            if (result.Customers.Count > 0)
                            {
                                newViewModel.ImportResult.Results.Add(result);
                            }
                            else
                            {
                                //Add the no match customers to session
                                newViewModel.ImportResult.NoMatchResults.Add(result);
                            }
                        }

                        //Store the no match customers
                        SessionData.Put("noMatchCustomers", newViewModel.ImportResult.NoMatchResults);
                        //bool isExist = _customerSearchBusinessManager.CheckForOrderHistory(null, viewModel.SearchCriteria.StartDate, viewModel.SearchCriteria.EndDate, lstBPCSCustNo);

                        //Figure out what to redirect to hear..


                        //resultMessage = 
                        //return Json(new { ResultMessage = "Details saved successfully, processed files will be sent to your email." });
                    }
                    else if (newViewModel.ImportResult.IsImportSuccessful && !viewModel.SearchCriteria.IsCustomerSearch)
                    {
                        resultMessage = newViewModel.ImportResult.SuccessfulResultMessage;
                    }
                    else if(!newViewModel.ImportResult.IsImportSuccessful)
                    {
                        string errorMessage = string.Empty;

                        //Get the error message thrown
                        if (!string.IsNullOrEmpty(newViewModel.ImportResult.FileFormattingMessage))
                        {
                            errorMessage = newViewModel.ImportResult.FileFormattingMessage;
                        }
                        else if (!string.IsNullOrEmpty(newViewModel.ImportResult.FileNotAccessibleMessage))
                        {
                            errorMessage = newViewModel.ImportResult.FileNotAccessibleMessage;
                        }
                        else if (!string.IsNullOrEmpty(newViewModel.ImportResult.FileRenameMessage))
                        {
                            errorMessage = newViewModel.ImportResult.FileRenameMessage;
                        }
                        else if (!string.IsNullOrEmpty(newViewModel.ImportResult.SaveFileMessage))
                        {
                            errorMessage = newViewModel.ImportResult.SaveFileMessage;
                        }
                        else
                        {
                            errorMessage = "An error occurred, please try again.";
                        }

                        resultMessage = errorMessage;
                    }
                }
            }
            else
            {
                resultMessage = "No file selected. Please select a file and try again.";
            }

            return Json(new
            {
                ViewModel = newViewModel,
                ResultMessage = resultMessage,
                IsSuccessful = newViewModel.ImportResult.IsImportSuccessful
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetImportSpreadsheetRowCount()
        {
            //Store checkbox values in session
            bool isCustomerSearch = Convert.ToBoolean(Request.Params["isCustomerSearch"]);
            bool isAproSearch = Convert.ToBoolean(Request.Params["isAproSearch"]);
            SessionData.Put("isCustomerSearch", isCustomerSearch);
            SessionData.Put("isAproSearch", isAproSearch);

            bool isSuccessful = true;
            HttpPostedFileBase importFile = null;
            int worksheetRowCount = 0;
            try
            {
                if (Request.Files.Count > 0)
                {
                    foreach (string file in Request.Files)
                    {
                        importFile = Request.Files[file];
                    }
                }

                CustomerSearchViewModel newViewModel = new CustomerSearchViewModel();
                string fileName = importFile.FileName;
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    //Store in session
                    SessionData.Put("importFile", importFile);

                    Logger.Info("Getting import spreadsheet row count...");
                    XLWorkbook workbook = new XLWorkbook(fileName);

                    if (workbook != null)
                    {
                        IXLWorksheet worksheet = workbook.Worksheet("Sheet1");
                        if (worksheet != null)
                        {
                            int worksheetColumnCount = worksheet.Columns().Count();
                            worksheetRowCount = worksheet.RowsUsed().Count();
                            SessionData.Put("importSpreadsheetRowCount", worksheetRowCount);
                            Logger.Info("Filename: " + fileName + " | Row count: " + worksheetRowCount);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                isSuccessful = false;
            }

            return Json(new {
                                rowCount = worksheetRowCount.ToString(),
                                IsSuccessful = isSuccessful
                            });
        }

        public PartialViewResult GetImportSpreadsheetBatchView()
        {
            return PartialView("~/Views/Customer/PartialViews/_ImportSpreadsheetForBatch.cshtml");
        }

        public PartialViewResult GetImportSpreadsheetBatchFieldsView()
        {
            return PartialView("~/Views/Customer/PartialViews/_ImportSpreadsheetBatchFields.cshtml");
        }

        public PartialViewResult GetImportSpreadsheetInitialView()
        {
            return PartialView("~/Views/Customer/PartialViews/_ImportSpreadsheetInitialView.cshtml");
        }

        [HttpPost]
        public ActionResult ExportSelectedToExcel(List<CustomerDetail> selectedRows)
        {
            string handle = Guid.NewGuid().ToString();
            MemoryStream memoryStream = new MemoryStream();
            ExportBusinessManager exportBusinessManager = new ExportBusinessManager();
            bool isExportMode = SessionData.Get<bool>("isExportMode");
            try
            {
                if (selectedRows != null)
                {
                    List<SearchCriteria> searchCriteria = new List<SearchCriteria>();
                    List<string> selectedCustomers;
                    string customerList = string.Empty;
                    bool CustomerDataOnly = false;

                    //Get data depending on if it was a customer search or import spreadsheet search
                    if (isExportMode)
                    {
                        ImportResult importResults = SessionData.Get<ImportResult>("importResults");
                        selectedRows = importResults.Results.SelectMany(x => x.Customers).Where(x => x.IsChecked).ToList();
                        selectedCustomers = selectedRows.Select(x => x.BPCSCustomerNumber.ToString()).ToList();
                        customerList = string.Join(",", selectedCustomers);

                        //Get the search criteria for the manual search and add it to the search criteria list
                        searchCriteria = GetSearchCriteriaForEachImportMatch(selectedRows, isExportMode, importResults);
                    }
                    else
                    {
                        CustomerDataOnly = true;
                        CustomerSearchResult customerSearchResult = SessionData.Get<CustomerSearchResult>("customerSearchResult");
                        selectedCustomers = selectedRows.Select(x => x.BPCSCustomerNumber.ToString()).ToList();
                        customerList = string.Join(",", selectedCustomers);

                        //Get the search criteria for the manual search and add it to the search criteria list
                        selectedRows = GetSearchCriteriaForEachMatch(selectedRows, isExportMode, customerSearchResult);
                    }

                    DataTable dt = new DataTable();
                    bool includeCustomerDataOnly;
                    if (selectedRows[0].ExportType == ExportType.WithoutHistory)
                    {
                        includeCustomerDataOnly = true;
                        dt = exportBusinessManager.ExportData(customerList, DateTime.Now.AddYears(-3), DateTime.Now, selectedRows, isExportMode, searchCriteria, includeCustomerDataOnly);
                    }
                    else if (selectedRows[0].ExportType == ExportType.WithHistory)
                    {
                        includeCustomerDataOnly = false;
                        dt = exportBusinessManager.ExportData(customerList, DateTime.Now.AddYears(-3), DateTime.Now, selectedRows, isExportMode, searchCriteria, includeCustomerDataOnly);
                    }
                    else if (selectedRows[0].ExportType == ExportType.NoMatch)
                    {
                        //exportBusinessManager.ExportDataForNomatch()
                    }

                    //Load the datatable in XL workbook object
                    using (XLWorkbook workbook = new XLWorkbook())
                    {
                        workbook.Worksheets.Add(dt, "Sheet1");
                        memoryStream = ExcelExtensions.GetStream(workbook);
                        workbook.SaveAs(memoryStream);
                        memoryStream.Position = 0;
                        TempData[handle] = memoryStream.ToArray();
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at ExportSelectedToExcel() " + exception);
            }

            return new JsonResult
            {
                Data = new { FileGuid = handle, FileName = "TestOutput.xlsx" }
            };
        }

        [HttpPost]
        public ActionResult ExportNoMatchToExcel()
        {
            string handle = Guid.NewGuid().ToString();
            MemoryStream memoryStream = new MemoryStream();
            ExportBusinessManager exportBusinessManager = new ExportBusinessManager();
            List<SearchResult> noMatchCustomers = SessionData.Get<List<SearchResult>> ("noMatchCustomers");
            try
            {
                DataTable dt = exportBusinessManager.ExportDataForNomatch(noMatchCustomers);

                //Load the datatable in XL workbook object
                using (XLWorkbook workbook = new XLWorkbook())
                {
                    workbook.Worksheets.Add(dt, "Sheet1");
                    memoryStream = ExcelExtensions.GetStream(workbook);
                    workbook.SaveAs(memoryStream);
                    memoryStream.Position = 0;
                    TempData[handle] = memoryStream.ToArray();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at ExportNoMatchToExcel() " + exception);
            }

            return new JsonResult
            {
                Data = new { FileGuid = handle, FileName = "TestOutput.xlsx" }
            };
        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }
        #endregion

        #region PRIVATE METHODS
        /// <summary>
        /// Method to get the search criteria for each of the import matches
        /// </summary>
        /// <param name="selectedRows"></param>
        /// <param name="isExportMode"></param>
        /// <param name="results"></param>
        /// <returns>Returns List<SearchCriteria></returns>
        private List<SearchCriteria> GetSearchCriteriaForEachImportMatch(List<CustomerDetail> selectedRows, bool isExportMode, ImportResult results)
        {
            var searchCriteriaList = new List<SearchCriteria>();
            if (isExportMode)
            {
                foreach (var item in selectedRows)
                {
                    foreach (var result in results.Results)
                    {
                        if (item.ReferenceNumber == result.Criteria.ReferenceNumber)
                        {
                            result.Criteria.DistributorIdForExport = item.DistributorID;
                            if (!searchCriteriaList.Contains(result.Criteria))
                            {
                                searchCriteriaList.Add(result.Criteria);
                            }
                        }
                    }
                }
            }

            return searchCriteriaList;
        }

        /// <summary>
        /// Method to get the search criteria for each customer search match
        /// </summary>
        /// <param name="selectedRows"></param>
        /// <param name="isExportMode"></param>
        /// <param name="results"></param>
        /// <returns>Returns List<SearchCriteria></returns>
        private List<CustomerDetail> GetSearchCriteriaForEachMatch(List<CustomerDetail> selectedRows, bool isExportMode, CustomerSearchResult results)
        {
            foreach (var row in selectedRows)
            {
                string refNum = !string.IsNullOrWhiteSpace(row.ReferenceNumber) ? row.ReferenceNumber.ToString() : string.Empty;
                foreach (var result in results.Customers)
                {
                    if (refNum == result.ReferenceNumber)
                    {
                        row.DistributorID = result.DistributorID;
                        row.SearchCriteria = result.SearchCriteria;
                        row.NameMatch = result.NameMatch;
                        row.AddressMatch = result.AddressMatch;
                        row.PhoneMatch = result.PhoneMatch;
                    }
                }
            }

            return selectedRows;
        }

        /// <summary>
        /// Reads the excel file and returns list of <see cref="SearchCriteriaViewModel"/>.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="isCustomerSearchMode">todo: describe isCustomerSearchMode parameter on ReadExcelFile</param>
        /// <param name="importRequest">todo: describe importRequest parameter on ReadExcelFile</param>
        /// <returns>A list of <see cref="SearchCriteriaViewModel"/> parsed from excel file.</returns>
        private CustomerSearchViewModel ReadExcelFile(string fileName, bool isCustomerSearchMode, ImportRequest importRequest) //Added isCustomerSearchMode as part of ALM 2436
        {
            Logger.Info("Beginning Read Excel File...");
            Logger.Info("File Name: " + fileName);
            Logger.Info("IsCustomerSearchMode: " + isCustomerSearchMode);
            XLWorkbook workbook = null;
            ImportResult importResult = new ImportResult();
            CustomerSearchViewModel viewModel = new CustomerSearchViewModel();

            bool isExport = false;
            int searchCountLimit = Convert.ToInt32(ConfigurationSettings.AppSettings["SearchCountLimit"].ToString()); //Importsearchlimit ALM 2393

            List<SearchCriteria> searchCriterias = new List<SearchCriteria>();
            try
            {
                Logger.Info("Reading values from excel file...");
                workbook = new XLWorkbook(fileName);

                if (workbook != null)
                {
                    Logger.Info("Excel workbook is not null...");

                    //Check column count
                    IXLWorksheet worksheet = workbook.Worksheet("Sheet1");
                    if (worksheet != null)
                    {
                        int worksheetColumnCount = worksheet.Columns().Count();
                        int worksheetRowCount = worksheet.RowsUsed().Count();
                        viewModel.ImportRequest.WorksheetRowCount = worksheetRowCount;

                        Logger.Info("Column Count: " + worksheetColumnCount.ToString());
                        Logger.Info("Row Count: " + worksheetRowCount.ToString());

                        if (worksheetColumnCount < 8 && !isCustomerSearchMode)
                        {
                            importResult.FileFormattingMessage = "Please check excel file format & try again";
                            importResult.IsImportSuccessful = false;
                        }
                        else if (worksheetColumnCount > 3 && isCustomerSearchMode)
                        {
                            importResult.FileFormattingMessage = "Please check excel file format & try again";
                            importResult.IsImportSuccessful = false;
                        }
                        else if (worksheetRowCount > searchCountLimit && !isCustomerSearchMode)
                        {
                            isExport = true;
                            List<string> lstFileNames = _customerSearchBusinessManager.GetFileNames();
                            if (System.IO.File.Exists(fileName))
                            {
                                Logger.Info("Copying file: " + fileName);
                                ImportFile importFile = ExcelExtensions.CopyFile(fileName, lstFileNames);

                                if (importFile.IsCopySuccessful && ImportRequest.FileNameCount > 0)
                                {
                                    //Update the file name with the safe file name in case the file was renamed
                                    importRequest.FileName = importFile.SafeFileName;
                                }
                                if (!importFile.IsCopySuccessful)
                                {
                                    importResult.SaveFileMessage = "Unable to Save file details, Please contact support team for further assistance.";
                                    importResult.IsImportSuccessful = false;
                                }
                                else
                                {
                                    importRequest.UserID = Environment.UserName;
                                    importRequest = _customerSearchBusinessManager.InsertImportFileInfo(importRequest);

                                    if (String.IsNullOrEmpty(importRequest.JobID))
                                    {
                                        importResult.SaveFileMessage = "Error while saving MailAddress details, Please contact support team for further assistance.";

                                        importResult.IsImportSuccessful = false;
                                        viewModel.ImportResult = importResult;
                                        return viewModel;
                                    }
                                    else
                                    {
                                        //If the insert customer info was successful then start the batch process
                                        ExportBusinessManager exportBusinessManager = new ExportBusinessManager();
                                        //exportBusinessManager.StartBatchProcess();

                                        importResult.SuccessfulResultMessage = "Details Saved successfully, processed files will be sent to your email.";
                                        importResult.IsImportSuccessful = true;
                                    }
                                }
                            }
                            else
                            {
                                importResult.FileNotAccessibleMessage = "Imported file is not accessible, please import again.";
                                importResult.IsImportSuccessful = false;
                            }
                        }
                        else if (worksheetRowCount <= searchCountLimit)
                        {
                            DataTable importSearchCriteria = ExcelExtensions.ImportExceltoDatatable(fileName);

                            int i = 2;
                            foreach (DataRow row in importSearchCriteria.Rows)
                            {
                                if (isCustomerSearchMode)
                                {
                                    string referenceNumber = row[1].ToString();
                                    string customerId = row[2].ToString();
                                    string externalCustomerId = row[3].ToString();

                                    //Create search criteria models.
                                    SearchCriteria searchCriteria = new SearchCriteria();
                                    searchCriteria.ReferenceNumber = referenceNumber != null ? referenceNumber.ToString() : string.Empty;
                                    searchCriteria.CustomerId = customerId != null ? customerId.ToString() : string.Empty;
                                    searchCriteria.ExternalCustomerId = externalCustomerId != null ? externalCustomerId.ToString() : string.Empty;

                                    if (!string.IsNullOrWhiteSpace(searchCriteria.ReferenceNumber))
                                    {
                                        searchCriterias.Add(searchCriteria);
                                    }
                                }
                                else
                                {
                                    string referenceNumber = row[0].ToString();
                                    string customerName = row[1].ToString();
                                    string address1 = row[2].ToString();
                                    string address2 = row[3].ToString();
                                    string city = row[4].ToString();
                                    string state = row[5].ToString();
                                    string zipCode = row[6].ToString();
                                    string phone = row[7].ToString();

                                    //Create search criteria models.
                                    SearchCriteria searchCriteria = new SearchCriteria();
                                    searchCriteria.ReferenceNumber = referenceNumber != null ? referenceNumber.ToString() : string.Empty;
                                    searchCriteria.Name = customerName != null ? customerName.ToString() : string.Empty;
                                    searchCriteria.Address1 = address1 != null ? address1.ToString() : string.Empty;
                                    searchCriteria.Address2 = address2 != null ? address2.ToString() : string.Empty;
                                    searchCriteria.City = city != null ? city : string.Empty;
                                    searchCriteria.State = state != null ? state.ToString() : string.Empty;
                                    searchCriteria.Zip = zipCode != null ? zipCode.ToString() : string.Empty;
                                    searchCriteria.Phone = phone != null ? phone.ToString() : string.Empty;

                                    if (!string.IsNullOrWhiteSpace(searchCriteria.ReferenceNumber))
                                    {
                                        searchCriterias.Add(searchCriteria);
                                    }
                                }
                            }

                            importResult.IsImportSuccessful = true;
                            viewModel.SearchCriterias = searchCriterias;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at ReadExcelFile() " + exception);
            }
            finally
            {
                //Dispose of the ClosedXml resources
                if (workbook != null)
                {
                    workbook.Dispose();
                }
            }

            viewModel.ImportResult = importResult;

            return viewModel;
        }

        /// <summary>
        /// Validates the search criteria for import.
        /// </summary>
        /// <param name="searchCriteria">The criteria.</param>
        /// <returns><c>true</c> if search criteria is valid, otherwise <c>false</c>.</returns>
        private bool ValidateSearchCriteriaForImport(SearchCriteria searchCriteria)
        {
            bool isValid = true;

            if (searchCriteria != null)
            {
                //// Validate all fields.
                if (!string.IsNullOrWhiteSpace(searchCriteria.Name) && searchCriteria.Name.Length > 30)
                {
                    isValid = false;
                }
                else if (!string.IsNullOrWhiteSpace(searchCriteria.Address1) && searchCriteria.Address1.Length > 30)
                {
                    isValid = false;
                }
                else if (!string.IsNullOrWhiteSpace(searchCriteria.Address2) && searchCriteria.Address2.Length > 30)
                {
                    isValid = false;
                }
                else if (!string.IsNullOrWhiteSpace(searchCriteria.City) && searchCriteria.City.Length > 30)
                {
                    isValid = false;
                }
                else if (!string.IsNullOrWhiteSpace(searchCriteria.State) && searchCriteria.State.Length > 2)
                {
                    isValid = false;
                }
                else if (!string.IsNullOrWhiteSpace(searchCriteria.Zip) && searchCriteria.Zip.Length > 9)
                {
                    isValid = false;
                }
                else if (!string.IsNullOrWhiteSpace(searchCriteria.Phone) && !searchCriteria.Phone.IsPhoneNumber())
                {
                    isValid = false;
                }

                //// Log if not valid.
                if (!isValid)
                {
                    Logger.Info(string.Format("Invalid search criteria for import. Criteria - Reference Number : {0}, Customer Name : {1}, Address1 : {2}, Address2 : {3}, City : {4}, Zip - {5}, Phone - {6}", searchCriteria.ReferenceNumber, searchCriteria.Name, searchCriteria.Address1, searchCriteria.Address2, searchCriteria.City, searchCriteria.State, searchCriteria.Zip, searchCriteria.Phone));
                }
            }

            return isValid;
        }

        /// <summary>
        /// Method to validate start and end dates
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Returns true or false</returns>
        private bool ValidateStartEndDate(DateTime startDate, DateTime endDate)
        {
            return true;
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validates the search criteria.
        /// </summary>
        /// <param name="searchCriteria">The search criteria.</param>
        /// <returns>Returns true or false</returns>
        private bool ValidateSearchCriteria(SearchCriteria searchCriteria)
        {
            bool isValid = true;
            string errorMessage = string.Empty;

            if (searchCriteria != null)
            {
                _searchCriteria = new List<SearchCriteria>();
                _searchCriteria.Add(searchCriteria);

                //// Validate phone number.
                if (!string.IsNullOrWhiteSpace(searchCriteria.Phone) && !searchCriteria.Phone.IsPhoneNumber())
                {
                    errorMessage = string.Format("Please enter valid phone number.");
                    isValid = false;
                }
                else if (searchCriteria.IsAproSearch)
                {
                    bool areAllFieldsPresent = !string.IsNullOrWhiteSpace(searchCriteria.Name) && !string.IsNullOrWhiteSpace(searchCriteria.Address) && !string.IsNullOrWhiteSpace(searchCriteria.City) && !string.IsNullOrWhiteSpace(searchCriteria.State) && !string.IsNullOrWhiteSpace(searchCriteria.Zip);
                    bool anyFieldsPresent = !string.IsNullOrWhiteSpace(searchCriteria.Name) || !string.IsNullOrWhiteSpace(searchCriteria.Address) || !string.IsNullOrWhiteSpace(searchCriteria.City) || !string.IsNullOrWhiteSpace(searchCriteria.State) || !string.IsNullOrWhiteSpace(searchCriteria.Zip);

                    //// Phone number is present including other fields.
                    if (!string.IsNullOrWhiteSpace(searchCriteria.Phone) && (!areAllFieldsPresent && anyFieldsPresent))
                    {
                        //if (MessageBox.Show("Account protection will run only the phone number.", UIMessages.Validation_Information, MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                        //{
                        //    return false;
                        //}
                        //else
                        //{
                        //    searchCriteria.ShouldSearchForPhoneOnly = true;
                        //}
                    }
                    else if (string.IsNullOrWhiteSpace(searchCriteria.Phone) && (string.IsNullOrWhiteSpace(searchCriteria.Name) || string.IsNullOrWhiteSpace(searchCriteria.Address) || string.IsNullOrWhiteSpace(searchCriteria.City) || string.IsNullOrWhiteSpace(searchCriteria.State) || string.IsNullOrWhiteSpace(searchCriteria.Zip)))
                    {
                        //// Distributor phone is empty and not all the other fields are entered.
                        isValid = false;
                        errorMessage = string.Format("Customer Name, Address, City, State, Zip Code OR Phone Number are required for account protection search.");
                    }
                }
            }

            //// Show popup.
            if (!isValid)
            {
                //MessageBox.Show(errorMessage, UIMessages.Validation_Error, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return isValid;
        }

        /// <summary>
        /// Method to get a list of state codes
        /// </summary>
        /// <returns>Return list of state codes</returns>
        private List<StateCode> GetStates()
        {
            List<StateCode> states = new List<StateCode>();
            states.AddRange(_customerSearchBusinessManager.GetStates());

            //Add default value to states list for display purposes
            states.Insert(0, new StateCode
            {
                Code = "",
                Name = "State / Province"
            });

            return states;
            //Cache the states list
            //HttpContext.Cache["StatesList"] = _viewModel.SearchCriteria.States;
        }
        #endregion
    }
}