﻿$(document).on('change', '#chkBoxSelectAllRows', function () {
    var selectAllRowsCheckboxValue = this.checked;

    $('#resultsTable #SearchResultsHeaderRow').each(function () {
        var currentRowCheckbox = $(this).find('#selectRowCheckbox');
        var currentRowCheckboxValue = currentRowCheckbox.prop('checked');

        //If the current checkbox value doesn't equal the select all checkbox value then change current checkbox value
        if (selectAllRowsCheckboxValue != currentRowCheckboxValue) {
            currentRowCheckbox.prop('checked', selectAllRowsCheckboxValue)
        }
    });
});