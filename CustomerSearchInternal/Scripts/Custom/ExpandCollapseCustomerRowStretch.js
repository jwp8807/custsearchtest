﻿$("body").on("click", "#resultsTableStretched #btnExpandCollapse", function () {
    //Expand or collapse rows
    //rowDetails.slideToggle("fast");
    //rowOrderHistoryButton.slideToggle("fast");
    orderHistoryRow.slideToggle("fast");
});

$("body").on("click", "#resultsTableStretched #btnExpandCollapse", function () {
    var row = $(this).closest("tr");
    var backgroundColor = row.css("background-color");
    var hiddenId = row.find("#hiddenId").val();
    var bpcsCustomerNum = row.find("#bpcsCustomerNumber").text();
    var orderHistoryRow = $("#" + hiddenId + ".orderHistoryRow");
    orderHistoryRow.css("background-color", backgroundColor);

    alert(backgroundColor);
    alert(hiddenId);

    $.ajax({
        type: 'POST',
        url: '/Customer/GetOrderHistory',
        data: {
            'bpcsCustomerNum': bpcsCustomerNum,
            'startDate': '2015-01-24',
            'endDate': '2018-01-24'
        },
        success: function (result) {
            //$("#" + orderHistoryRowIndex + ".orderHistoryRow").show();
            $("#orderHistoryPartialView." + orderHistoryRowIndex + "").html(result);
            $("#" + orderHistoryRowIndex + ".loaderOrderHistory").hide();
            currentGetOrderHistoryButton.hide();
        }
    });
});