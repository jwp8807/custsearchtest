﻿namespace CustomerSearchInternal.ViewModels
{
    #region NAMESPACES
    using CustomerSearchInternal.Models;
    using CustomerSearchInternal.Models.Request;
    using System.Collections.Generic;
    using CustomerSearchInternal.Enum;
    #endregion

    public class CustomerSearchViewModel : ModelBase
    {
        #region FIELDS
        private SearchCriteria _searchCriteria;
        private List<SearchCriteria> _searchCriterias;
        private CustomerSearchResult _customerSearchResult;
        private List<StateCode> _states;
        private ImportRequest _importRequest;
        private List<OrderHistory> _orderHistory;
        private List<OrderDetail> _orderDetail;
        private ImportResult _importResult;
        #endregion

        #region PROPERTIES
        public ResultsViewMode ResultsViewMode { get; set; }

        public string CurrentBPCSCustomerNumber { get; set; }

        public ImportResult ImportResult
        {
            get
            {
                if (_importResult == null)
                {
                    _importResult = new ImportResult();
                }

                return _importResult;
            }

            set
            {
                _importResult = value;
            }
        }

        public List<OrderHistory> OrderHistory
        {
            get
            {
                if (_orderHistory == null)
                {
                    _orderHistory = new List<OrderHistory>();
                }

                return _orderHistory;
            }
            set
            {
                _orderHistory = value;
            }
        }

        public List<OrderDetail> OrderDetail
        {
            get
            {
                if (_orderDetail == null)
                {
                    _orderDetail = new List<OrderDetail>();
                }

                return _orderDetail;
            }
            set
            {
                _orderDetail = value;
            }
        }

        /// <summary>
        /// The import request
        /// </summary>
        public ImportRequest ImportRequest
        {
            get
            {
                if (_importRequest == null)
                {
                    _importRequest = new ImportRequest();
                }

                return _importRequest;
            }
            set
            {
                _importRequest = value;
            }
        }

        //public static string CurrentUserId
        //{
        //    get
        //    {
        //        return SessionData.Get<string>("UserName");
        //    }
        //}

        /// <summary>
        /// The Customer Search Result property
        /// </summary>
        public CustomerSearchResult CustomerSearchResult
        {
            get
            {
                if (_customerSearchResult == null)
                {
                    _customerSearchResult = new CustomerSearchResult();
                }

                return _customerSearchResult;
            }
            set
            {
                _customerSearchResult = value;
            }
        }

        /// <summary>
        /// The Search Criteria property
        /// </summary>
        public SearchCriteria SearchCriteria
        {
            get
            {
                if (_searchCriteria == null)
                {
                    _searchCriteria = new SearchCriteria();
                }

                return _searchCriteria;
            }
            set
            {
                _searchCriteria = value;
            }
        }

        /// <summary>
        /// The Search Criterias property
        /// </summary>
        public List<SearchCriteria> SearchCriterias
        {
            get
            {
                if (_searchCriterias == null)
                {
                    _searchCriterias = new List<SearchCriteria>();
                }

                return _searchCriterias;
            }
            set
            {
                _searchCriterias = value;
            }
        }

        public List<StateCode> States
        {
            get
            {
                if (_states == null)
                {
                    _states = new List<StateCode>();
                }

                return _states;
            }
            set
            {
                _states = value;
            }
        }

        public bool ShouldSearchForPhoneOnly { get; set; }
        public bool IsCustomerSearchMode { get; internal set; }
        #endregion
    }
}