﻿namespace CustomerSearchInternal.Session
{
    #region NAMESPACES
    using System;
    using System.Web;
    #endregion

    public class SessionData
    {
        #region FIELDS
        static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SessionData));
        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// Method to get the session data by key name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            object value = HttpContext.Current.Session[key];

            if (value == null)
                return default(T);

            try
            {
                return (T)value;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return default(T);
            }
        }

        /// <summary>
        /// Method to put value in session by key name
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Put(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }

        #endregion
    }
}