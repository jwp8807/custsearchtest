﻿$("body").on("click", "#btnExportToExcel", function () {
    //Set the export type
    var exportType = "";
    var exportButtonText = $(this).text();
    if (exportButtonText == "Export With History") {
        exportType = "WithHistory";
    }
    else if (exportButtonText == "Export Without History") {
        exportType = "WithoutHistory";
    }
    else if (exportButtonText == "Export No Match") {
        exportType = "NoMatch";
    }

    var selectedRows = [];
    var customerDetail = [];
    $('#resultsTable #SearchResultsHeaderRow').each(function () {
        var selectRowCheckbox = $(this).find('#selectRowCheckbox');
        //Add the fields to the customerDataString in pipe delimited format
        if (selectRowCheckbox.prop('checked') === true) {
            var row = $(this).closest("tr");
            var hiddenId = row.find("#hiddenId").val();
            var customerDetailsRow = $('#' + hiddenId + '.detailsRow').closest("tr");

            //Get all the needed fields
            var distId = customerDetailsRow.find('.distributorId').text();
            var distName = customerDetailsRow.find('.distributorName').text();
            var distPhone = customerDetailsRow.find('.distributorPhone').text();
            var distFax = customerDetailsRow.find('.distributorFax').text();
            var distEmail = customerDetailsRow.find('.distributorEmail').text();
            var billName = customerDetailsRow.find('.billName').text();
            var billAddr1 = customerDetailsRow.find('.billAddr1').text();
            var billAddr2 = customerDetailsRow.find('.billAddr2').text();
            var billCity = customerDetailsRow.find('.billCity').text();
            var billState = customerDetailsRow.find('.billState').text();
            var billZip = customerDetailsRow.find('.billZip').text();
            var billPhone = customerDetailsRow.find('.billPhone').text();
            var shipName = customerDetailsRow.find('.shipName').text();
            var shipAddr1 = customerDetailsRow.find('.shipAddr1').text();
            var shipAddr2 = customerDetailsRow.find('.shipAddr2').text();
            var shipCity = customerDetailsRow.find('.shipCity').text();
            var shipState = customerDetailsRow.find('.shipState').text();
            var shipZip = customerDetailsRow.find('.shipZip').text();
            var firstOrderDate = customerDetailsRow.find('.firstOrderDate').text();
            var lastorderDate = customerDetailsRow.find('.lastorderDate').text();
            var lastInvoiceDate = customerDetailsRow.find('.lastInvoiceDate').text();
            var customerStatus = customerDetailsRow.find('.customerStatus').text();
            var bpcsCustNum = customerDetailsRow.find('.bpcsCustNum').text();
            var externalCustNum = customerDetailsRow.find('.externalCustNum').text();

            customerDetail =
                {
                    IsChecked: true,
                    ExportType: exportType,
                    DistributorID: distId,
                    DistributorName: distName,
                    DistributorPhoneNumber: distPhone,
                    DistributorFaxNumber: distFax,
                    DistributorEmailAddress: distEmail,
                    BillToName: billName,
                    BillToAddress1: billAddr1,
                    BillToAddress2: billAddr2,
                    BillToCity: billCity,
                    BillToState: billState,
                    BillToZip: billZip,
                    BillToPhone: billPhone,
                    ShipToName: shipName,
                    ShipToAddress1: shipAddr1,
                    ShipToAddress2: shipAddr2,
                    ShipToCity: shipCity,
                    ShipToState: shipState,
                    ShipToZip: shipZip,
                    FirstOrderDate: firstOrderDate,
                    LastOrderDate: lastorderDate,
                    LastInvoiceDate: lastInvoiceDate,
                    CustomerStatus: customerStatus,
                    BPCSCustomerNumber: bpcsCustNum,
                    ExternalCustomerNumber: externalCustNum
                };

            selectedRows.push(customerDetail);
        }
    });

    //exportRequestArray = JSON.stringify({ 'exportRequestArray': exportRequestArray });


		//var myObj = [
		//	{ 'fstName': 'name 1', 'lastName': 'last name 1', 'age': 32 }
		//	, { 'fstName': 'name 2', 'lastName': 'last name 1', 'age': 33 }
		//];
        var postData = JSON.stringify({ 'selectedRows': selectedRows });

    $.ajax({
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: 'POST',
        url: '/Customer/ExportSelectedToExcel/',
        data: postData,
        success: function (data) {
            //Get the export response and then force download prompt
            var response = JSON.parse(JSON.stringify({ 'data': data }));
            window.location = '/Customer/Download?fileGuid=' + response.data.FileGuid + '&filename=' + response.data.FileName;
        }
    });
});