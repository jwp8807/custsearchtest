﻿$("body").on("click", "#btnClear", function () {
	//Clear all the fields
	$('#txtName').val('');
	$('#txtAddress').val('');
	$('#txtCity').val('');
	$('#dropDownStates').val('State / Province');
	$('#txtZip').val('');
	$('#txtPhone').val('');
	$('#txtDistributorId').val('');
	$('#txtCustomerId').val('');
	$('#txtExternalCustomerId').val('');
});