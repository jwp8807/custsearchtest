﻿$("body").on("click", "#btnExportNoMatch", function () {
    $.ajax({
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        url: '/Customer/ExportNoMatchToExcel/',
        success: function (data) {
            //Get the export response and then force download prompt
            var response = JSON.parse(JSON.stringify({ 'data': data }));
            window.location = '/Customer/Download?fileGuid=' + response.data.FileGuid + '&filename=' + response.data.FileName;
        }
    });
});