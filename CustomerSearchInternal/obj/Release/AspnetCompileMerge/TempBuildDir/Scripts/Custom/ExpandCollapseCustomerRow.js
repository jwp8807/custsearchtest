﻿$("body").on("click", "#resultsTable #btnExpandCollapse", function () {
    //Get the current row selected and then the needed components
    var row = $(this).closest("tr");
    var backgroundColor = row.css("background-color");
    var hiddenId = row.find("#hiddenId").val();
    var rowDetails = $("#" + hiddenId + ".detailsRow");
    var rowOrderHistoryButton = $("#orderHistoryButtonRow." + hiddenId + "");
    var rowOrderHistoryDetails = $("#" + hiddenId + ".orderHistoryRow");

    //Set the background color of rows
    rowDetails.css("background-color", backgroundColor);
    rowOrderHistoryButton.css("background-color", backgroundColor);
    rowOrderHistoryDetails.css("background-color", backgroundColor);
    $("#orderHistoryPartialViewParentTable #orderHistoryRow").css("background-color", backgroundColor);

    //Expand or collapse rows
    rowDetails.slideToggle("fast");
    rowOrderHistoryButton.slideToggle("fast");
    rowOrderHistoryDetails.slideToggle("fast");
});

