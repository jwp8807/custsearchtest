﻿//Submit import spreadsheet button clicked, directs the file to either batch or getting search results
$("body").on("click", "#btnSubmitImportSpreadsheet", function () {
	var file = document.getElementById('ImportSpreadsheetFile'),
		formData = new FormData();

	if (file.files.length > 0) {
		for (var i = 0; i < file.files.length; i++) {
			formData.append('file-' + i, file.files[i]);
		}
	}

	//Add the isCustomerSearch value
	var isAproSearch = $("#checkBoxAproSearch").is(':checked');
	var isCustomerSearch = $("#checkBoxCustomerSearch").is(':checked');

	formData.append('isAproSearch', isAproSearch);
	formData.append('isCustomerSearch', isCustomerSearch);

	$.ajax({
		url: '/Customer/GetImportSpreadsheetRowCount',
		type: 'POST',
		data: formData,
		dataType: 'json',
		contentType: false,
		processData: false,
		success: function (data) {

			if (data.IsSuccessful) {
				if (data.rowCount > 100) {
					//Get the partial view for processing via Batch
					$.get('/Customer/GetImportSpreadsheetBatchView', function (importSpreadsheetBatchView) {
						$("#importDivContainer").replaceWith(importSpreadsheetBatchView);
					});
				}
				else {
					//Submit the spreadsheet for processing
					$.ajax({
						url: '/Customer/SubmitImportSpreadsheet',
						type: 'POST',
						data: formData,
						dataType: 'json',
						contentType: false,
						processData: false,
						success: function (response) {
							//Call the ImportSearchResults in order to display the results in the view
							$.ajax({
								url: '/Customer/DisplayImportSearchResults',
								type: 'POST',
								data: { viewModel: response.ViewModel },
                                success: function (resultsView) {
									$("#importDivContainer").replaceWith("<p style='font-weight:bold';>Displaying results below, please wait...</p>");
									$("#resultsDivContainer").replaceWith(resultsView);

									//Hide the pop up
									$("#importPopUp").css("display", "none");
								}
							});
						}
					});
				}
			}
			else {
				$("#importDivContainer").replaceWith("<p style='color:red; font-weight: bold;'>An error occurred. Please try again.</p>");
			}
		}
	});
});

//Should process for batch button "No" clicked
$("body").on("click", "#btnShouldProcessViaBatchNo", function () {
	//Reset the popup to initial import state
	$.get('/Customer/GetImportSpreadsheetInitialView', function (importSpreadsheetInitialView) {
		$("#importDivContainer").replaceWith(importSpreadsheetInitialView);
	});

	//Hide the pop up
	$("#importPopUp").css("display", "none");
});

//Should process for batch button "Yes" clicked
$("body").on("click", "#btnShouldProcessViaBatchYes", function () {
	//Get the import spreadsheet for batch fields view
	$.get('/Customer/GetImportSpreadsheetBatchFieldsView', function (importSpreadsheetBatchFieldsView) {
        $("#importDivContainer").replaceWith(importSpreadsheetBatchFieldsView);
	});
});

//Exclude distributors dropdown value changed
$("body").on("change", "#dropDownExcludeDist", function () {
    alert(change);
	var selectedValue = this.value;

	if (selectedValue == "true") {
		$("#distToExcludeRow").show();
		$("#distToExcludeMessageRow").show();
	}
	else {
		$('#distToExcludeRow').hide();
		$("#distToExcludeMessageRow").hide();
	}
});

//Final submit button to submit file to batch processing
$("body").on("click", "#btnSubmitImportSpreadsheetForBatch", function () {
	var dropDownIncludeHistory = $('#dropDownIncludeHistory :selected').text();
	var dropDownExcludeDist = $('#dropDownExcludeDist :selected').text();
	var emailAddress = $("#txtEmailAddress").val();
	var distributorsToExclude = "";

	if (dropDownExcludeDist.toUpperCase() == "YES") {
		distributorsToExclude = $("#txtDistToExclude").val();
	}

	$.ajax({
		url: '/Customer/SubmitImportSpreadsheet',
		type: 'GET',
		data: {
			"emailAddress": emailAddress,
			"dropDownIncludeHistory": dropDownIncludeHistory,
			"dropDownExcludeDist": dropDownExcludeDist,
			"distributorsToExclude": distributorsToExclude,
		},
		success: function (data) {
			var color = "";
			if (data.IsSuccessful) {
				color = "green";
			}
			else {
				color = "red";
			}
            $("#importDivContainer").replaceWith("<p style='margin-top:20px; font-weight:bold;color:" + color + ";'>" + data.ResultMessage + "</p>");

            //Hide the loader
            $("body #loaderImportSpreadsheet").css("display", "none");
		}
	});
});