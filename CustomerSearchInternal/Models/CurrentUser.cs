﻿namespace CustomerSearchInternal.Models
{
    #region NAMESPACES
    using CustomerSearchInternal.Session;
    using System.ComponentModel.DataAnnotations;
    #endregion

    public class CurrentUser
    {
        #region FIELDS
        private static CurrentUser _currentUser;
        #endregion

        #region PROPERTIES
        [Required(ErrorMessage = "User name is required")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool isAuthenticated { get; set; }

        public static CurrentUser CurrentUserModel
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = new CurrentUser();
                }

                return _currentUser;
            }
        }

        public static string CurrentUserId
        {
            get
            {
                return SessionData.Get<string>("UserName");
            }
        }
        #endregion

        #region METHODS
        /// <summary>
		/// Disposes the current user.
		/// </summary>
		public void DisposeCurrentUser()
        {
            _currentUser = new CurrentUser();
        }
        #endregion
    }
}