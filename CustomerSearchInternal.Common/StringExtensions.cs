﻿namespace CustomerSearchInternal.Common
{
    #region NAMESPACES
    using System.Text.RegularExpressions;
    #endregion

    public static class StringExtensions
    {
        #region METHODS
        /// <summary>
        /// This method is used to validate if a string value is a valid phone number
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsPhoneNumber(this string value)
        {
            return !string.IsNullOrWhiteSpace(value) && value.Length == 10 && Regex.IsMatch(value, @"^\d+$");
        }

        /// <summary>
        /// This method is added as part ALM 2393
        /// to validate email address
        /// </summary>
        /// <param name="mailID"></param>
        /// <returns><c>true</c>valid mail address<c>false</c>Invalid mail address</returns>
        public static bool IsValidEmailAddress(this string mailID)
        {
            //Regex regex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            Regex regex = new Regex(@"((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*");

            return regex.IsMatch(mailID);
        }

        /// <summary>
        /// Method used to validate if a string value is a valid distributor
        /// </summary>
        /// <param name="DistId"></param>
        /// <returns></returns>
        public static bool IsValidDistributors(this string DistId)
        {
            //Regex regex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            Regex regex = new Regex(@"^[a-zA-Z0-9]{4,4}$");

            return regex.IsMatch(DistId);
        }
        #endregion
    }
}
