﻿namespace CustomerSearchInternal.Common
{
    #region NAMESPACES
    using ClosedXML.Excel;
    using System;
    using System.IO;
    using System.Data;
    using CustomerSearchInternal.Logging;
    using System.Collections.Generic;
    using CustomerSearchInternal.Models;
    using System.Configuration;
    using System.Text.RegularExpressions;
    using CustomerSearchInternal.Models.Request;
    #endregion

    public static class ExcelExtensions
    {
        #region PUBLIC METHODS
        /// <summary>
        /// Method to take import spreadsheet and convert it to datatable
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>Returns datatable</returns>
        public static DataTable ImportExceltoDatatable(string filePath)
        {
            // Open the Excel file using ClosedXML.
            // Keep in mind the Excel file cannot be open when trying to read it
            using (XLWorkbook workBook = new XLWorkbook(filePath))
            {
                //Read the first Sheet from Excel file.
                IXLWorksheet workSheet = workBook.Worksheet(1);

                //Create a new DataTable.
                DataTable dt = new DataTable();

                //Loop through the Worksheet rows.
                bool firstRow = true;
                foreach (IXLRow row in workSheet.Rows())
                {
                    //Use the first row to add columns to DataTable.
                    if (firstRow)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            dt.Columns.Add(cell.Value.ToString());
                        }
                        firstRow = false;
                    }
                    else
                    {
                        //Add rows to DataTable.
                        dt.Rows.Add();
                        int i = 0;

                        foreach (IXLCell cell in row.Cells(row.FirstCellUsed().Address.ColumnNumber, row.LastCellUsed().Address.ColumnNumber))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                            i++;
                        }
                    }
                }

                return dt;
            }
        }

        /// <summary>
        /// Method to convert the byte array to Excel and save it to the specified path
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="filePath"></param>
        /// <returns>Returns true or false</returns>
        public static bool ConvertAndSaveExcelFile(byte[] bytes, string filePath)
        {
            bool isSuccessful = false;
            try
            {
                //Get content of the Excel file
                var ms = new MemoryStream(bytes, 0, bytes.Length);

                using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    //Write content of your memory stream into file stream
                    ms.WriteTo(fs);
                }

                isSuccessful = true;
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                Logger.Error("Error occurred at ConvertAndSaveExcelFile: \n" + ex);
            }

            return isSuccessful;
        }

        /// <summary>
        /// Method to create the datatable structure used for exporting
        /// </summary>
        /// <param name="customerDataOnly"></param>
        /// <param name="isForNoMatch"></param>
        /// <returns>Returns datatable</returns>
        public static DataTable GetDataTableStructureForExport(bool customerDataOnly, bool isForNoMatch)
        {
            DataTable dt = new DataTable();
            dt.TableName = "Export Data";

            if (!isForNoMatch)
            {
                dt.Columns.Add("Search Name", typeof(string));
                dt.Columns.Add("Search Address", typeof(string));
                dt.Columns.Add("Search City", typeof(string));
                dt.Columns.Add("Search State", typeof(string));
                dt.Columns.Add("Search Zip", typeof(string));
                dt.Columns.Add("Search Phone", typeof(string));
                dt.Columns.Add("Search Dist. ID", typeof(string));
                dt.Columns.Add("Customer Number", typeof(string));
                dt.Columns.Add("External Customer Number", typeof(string));
                dt.Columns.Add("Order History Date Range From", typeof(string));
                dt.Columns.Add("Order History Date Range To", typeof(string));
                dt.Columns.Add("Standardized Address Line", typeof(string));
                dt.Columns.Add("Standardized Zip", typeof(string));
                dt.Columns.Add("Permail Error Indicator", typeof(string));
                dt.Columns.Add("Reference Number", typeof(string));
                dt.Columns.Add("Distributor ID", typeof(string));
                dt.Columns.Add("Distributor Name", typeof(string));
                dt.Columns.Add("Distributor Phone#", typeof(string));
                dt.Columns.Add("Distributor Fax#", typeof(string));
                dt.Columns.Add("Distributor Email Address", typeof(string));
                dt.Columns.Add("Last Invoice Date", typeof(string));
                dt.Columns.Add("First Order Date", typeof(string));
                dt.Columns.Add("Last Order Date", typeof(string));
                dt.Columns.Add("Customer Status", typeof(string));
                dt.Columns.Add("BPCS Customer #", typeof(string));
                dt.Columns.Add("Ext. Customer #", typeof(string));
                dt.Columns.Add("Bill to Name", typeof(string));
                dt.Columns.Add("Bill to Addr1", typeof(string));
                dt.Columns.Add("Bill to Addr2", typeof(string));
                dt.Columns.Add("Bill to City", typeof(string));
                dt.Columns.Add("Bill to State", typeof(string));
                dt.Columns.Add("Bill to Zip", typeof(string));
                dt.Columns.Add("Bill to Phone", typeof(string));
                dt.Columns.Add("Cust Ship to Name", typeof(string));
                dt.Columns.Add("Cust Ship to Addr1", typeof(string));
                dt.Columns.Add("Cust Ship to Addr2", typeof(string));
                dt.Columns.Add("Cust Ship to City", typeof(string));
                dt.Columns.Add("Cust Ship to State", typeof(string));
                dt.Columns.Add("Cust Ship to Zip", typeof(string));
                dt.Columns.Add("Name Match", typeof(string));
                dt.Columns.Add("Addr Match", typeof(string));
                dt.Columns.Add("Phone Match", typeof(string));
                dt.Columns.Add("Rotation Exception", typeof(string));

                if (!customerDataOnly)
                {
                    //If not customer data only then add the order history columns
                    dt.Columns.Add("Order Date", typeof(string));
                    dt.Columns.Add("Order #", typeof(string));
                    dt.Columns.Add("Ext Order #", typeof(string));
                    dt.Columns.Add("Order Distributor ID", typeof(string));
                    dt.Columns.Add("PO #", typeof(string));
                    dt.Columns.Add("Invoice #", typeof(string));
                    dt.Columns.Add("Invoice Date", typeof(string));
                    dt.Columns.Add("Order Bill to Name", typeof(string));
                    dt.Columns.Add("Order Bill to Address 1", typeof(string));
                    dt.Columns.Add("Order Bill to Address 2", typeof(string));
                    dt.Columns.Add("Order Bill to City", typeof(string));
                    dt.Columns.Add("Order Bill to State", typeof(string));
                    dt.Columns.Add("Order Bill to Zip", typeof(string));
                    dt.Columns.Add("Order Ship To Name", typeof(string));
                    dt.Columns.Add("Order Ship to Address 1", typeof(string));
                    dt.Columns.Add("Order Ship to Address 2", typeof(string));
                    dt.Columns.Add("Order Ship to City", typeof(string));
                    dt.Columns.Add("Order Ship to State", typeof(string));
                    dt.Columns.Add("Order Ship to Zip", typeof(string));
                    dt.Columns.Add("Line #", typeof(string));
                    dt.Columns.Add("Transaction Type", typeof(string));
                    dt.Columns.Add("Prod Line", typeof(string));
                    dt.Columns.Add("Prod Code", typeof(string));
                    dt.Columns.Add("Prod Description", typeof(string));
                    dt.Columns.Add("Quantity", typeof(string));
                    dt.Columns.Add("Invoice Amount", typeof(string));
                    dt.Columns.Add("Forms Amount", typeof(string));
                    dt.Columns.Add("Commission Amount", typeof(string));
                    dt.Columns.Add("Margin Amount", typeof(string));
                    dt.Columns.Add("Freight Amount", typeof(string));
                    dt.Columns.Add("Vendor ID", typeof(string));
                    dt.Columns.Add("Vendor Name", typeof(string));
                    dt.Columns.Add("2014 Non-Integrated BAM Order History - FINANCIALS NOT VALIDATED", typeof(string));
                }
            }
            else
            {
                //Columns for No Match spreadsheet
                dt.Columns.Add("Reference Number", typeof(string));
                dt.Columns.Add("Customer Number", typeof(string));
                dt.Columns.Add("External Customer Number", typeof(string));
                dt.Columns.Add("No Match (Y/N)", typeof(string));
                dt.Columns.Add("Error (Y/N)", typeof(string));
            }

            return dt;
        }

        /// <summary>
        /// Method to get memory stream from Excel Workbook
        /// </summary>
        /// <param name="excelWorkbook"></param>
        /// <returns>Returns MemoryStream</returns>
        public static MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }


        /// <summary>
        /// Method to generate unique file name
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="lstFileNames"></param>
        /// <param name="inUse"></param>
        /// <returns>Returns string unique file name</returns>
        public static string NextUniqueFilename(string fileName, List<string> lstFileNames, Func<string, bool> inUse)
        {
            if (!inUse(fileName))
            {
                if (lstFileNames == null || (lstFileNames != null && !lstFileNames.Contains(fileName)))
                    // this filename has not been seen before, return it unmodified
                    return fileName;
            }
            // this filename is already in use, add " (n)" to the end
            var name = System.IO.Path.GetFileNameWithoutExtension(fileName);
            var extension = System.IO.Path.GetExtension(fileName);
            if (name == null)
            {
                throw new Exception("File name without extension returned null.");
            }
            const int max = 9999;
            for (var i = 1; i < max; i++)
            {
                var nextUniqueFilename = string.Format("{0}({1}){2}", name, i, extension);
                if (!inUse(nextUniqueFilename))
                {
                    if (lstFileNames == null || (lstFileNames != null && !lstFileNames.Contains(nextUniqueFilename)))
                    {
                        ++ImportRequest.FileNameCount;
                        return nextUniqueFilename;
                    }
                }
            }
            throw new Exception(string.Format("Too many files by this name. Limit: {0}", max));
        }

        /// <summary>
        /// This method is added as part of ALM 2393
        /// to copy customer searched file into shared path
        /// for Windows service batch processing.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="lstFileNames"></param>
        /// <returns><c>true</c>file copied,<c>false</c>if not copied</returns>
        public static ImportFile CopyFile(string fileName, List<string> lstFileNames)
        {
            ImportFile importFile = new ImportFile();
            string targetPath = ConfigurationManager.AppSettings["InputFilePath"].ToString();
            //CustomerInfo custObj = new CustomerInfo();
            try
            {
                string tempFileName = System.IO.Path.GetFileName(fileName);
                if (tempFileName.Contains(" "))
                {
                    tempFileName = Regex.Replace(tempFileName, @"\s+", "");
                    //MessageBox.Show("Imported file name contain spaces, we are removing them to generate processed files hyperlink without any break.", "Message");
                    Logger.Info(string.Format("Imported file name contain spaces, we are removing them to generate processed files hyperlink without any break. \"\n\"Updated file name - {0} ", tempFileName));
                }

                string safeName = string.Empty;
                if (Directory.Exists(targetPath))
                {
                    safeName = ExcelExtensions.NextUniqueFilename(tempFileName, lstFileNames, f => System.IO.File.Exists(System.IO.Path.Combine(targetPath, f)));
                    if (safeName.Length <= 50)
                    {
                        // Use Path class to manipulate file and directory paths.
                        //safeName = "\"" + safeName + "\"";
                        string sourceFile = fileName;//System.IO.Path.Combine(sourcePath, fileName);
                        string destFile = System.IO.Path.Combine(targetPath, safeName);
                        // To copy a file to another location
                        if (!System.IO.File.Exists(sourceFile))
                            Logger.Info("File Not accessible.");
                        System.IO.File.Copy(sourceFile, destFile, true);
                        importFile.SafeFileName = safeName;
                        importFile.IsCopySuccessful = true;
                    }
                    else
                    {
                        //MessageBox.Show(string.Format(safeName + " is greater than 50 characters. Please rename your file so it is less than 50 characters." +
                        //    Environment.NewLine + Environment.NewLine + "Note: .xlsx is added to your file name and included in the character count check, so please take that into consideration when renaming your file."), "File Name Exceeded Character Limit");
                        Logger.Info(safeName + " is greater than 50 characters.");
                        importFile.IsCopySuccessful = false;
                    }
                }
                else
                {
                    //MessageBox.Show("Directory is not accessible."+targetPath);
                    //MessageBox.Show(string.Format("You do not have access to the path \"{0}\", Please contact support team for further assistance.", targetPath));
                    Logger.Info("Directory not exists.");
                    importFile.IsCopySuccessful = false;
                }
            }
            catch (Exception exception)
            {
                //throw new Exception(exception.Message);
                //MessageBox.Show(string.Format("You do not have access to the path \"{0}\", Please contact support team for further assistance.", targetPath));
                Logger.Error("Error occurred at CopyFile() " + exception);
                importFile.IsCopySuccessful = false;
            }

            return importFile;
        }
        #endregion
    }
}
