﻿namespace CustomerSearchInternal.Interfaces
{
    #region NAMESPACES
    using CustomerSearchInternal.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    #endregion

    public interface IExportManager
    {
        /// <summary>
        /// Exports the customer data
        /// </summary>
        /// <param name="customerList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="selectedRows"></param>
        /// <param name="IsExportMode"></param>
        /// <param name="searchCriteria"></param>
        /// <param name="mainViewModel"></param>
        /// <param name="CustomerDataOnly"></param>
        DataTable ExportData(string customerList, DateTime startDate, DateTime endDate, List<CustomerDetail> selectedRows, bool isExportMode, List<SearchCriteria> searchCriteria, bool customerDataOnly = false);

        /// <summary>
        /// Exports the customer data for no matches
        /// </summary>
        /// <param name="noMatchCustomers"></param>
        /// <param name="isCustomerSearchMode"></param>
        /// <param name="importColumnCount"></param>
        DataTable ExportDataForNomatch(List<SearchResult> noMatchCustomers);

        /// <summary>
        /// Starts the Batch Process
        /// </summary>
        void StartBatchProcess();
    }
}
