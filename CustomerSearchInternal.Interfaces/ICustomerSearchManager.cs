﻿namespace CustomerSearchInternal.Interfaces
{
    #region NAMESPACES
    using CustomerSearchInternal.Models;
    using CustomerSearchInternal.Models.Request;
    using System;
    using System.Collections.Generic;
    #endregion

    public interface ICustomerSearchManager
    {
        /// <summary>
        /// Method to get the customer matches
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns>Returns CustomerSearchResult object</returns>
        CustomerSearchResult GetCustomerMatches(SearchCriteria searchCriteria);

        /// <summary>
        /// Method to get a list of state codes
        /// </summary>
        /// <returns>Returns List<string> of state codes</returns>
        List<StateCode> GetStates();

        /// <summary>
        /// Method to get a list of order history by customer id
        /// </summary>
        /// <param name="bpcsCustomerNum"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        List<OrderHistory> GetOrderHistory(string bpcsCustomerNum, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Method to get the order details
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Returns list of OrderDetail</returns>
        List<OrderDetail> GetOrderDetail(string bpcsCustomerNum, string invoiceNum, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets the file names
        /// </summary>
        /// <returns>Returns List<string></returns>
        List<string> GetFileNames();

        /// <summary>
        /// Method to insert the import request info into database for batch processing
        /// </summary>
        /// <param name="importRequest"></param>
        /// <returns>Returns ImportRequest object</returns>
        ImportRequest InsertImportFileInfo(ImportRequest importRequest);
    }
}
