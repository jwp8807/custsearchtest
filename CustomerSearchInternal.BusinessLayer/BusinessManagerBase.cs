﻿namespace CustomerSearchInternal.BusinessLayer
{
    #region NAMESPACES
    using CustomerSearchInternal.DataLayer;
    using CustomerSearchInternal.Interfaces;
    #endregion

    public class BusinessManagerBase
    {
        #region FIELDS
        private ICustomerSearchManager _customerSearchManager;
        private IExportManager _exportManager;
        #endregion

        #region PROPERTIES
        /// <summary>
        /// The customer search manager
        /// </summary>
        internal ICustomerSearchManager CustomerSearchManager
        {
            get
            {
                if (_customerSearchManager == null)
                {
                    _customerSearchManager = new CustomerSearchDataManager();
                }

                return _customerSearchManager;
            }
            set
            {

            }
        }

        /// <summary>
        /// The export manager
        /// </summary>
        internal IExportManager ExportManager
        {
            get
            {
                if (_exportManager == null)
                {
                    _exportManager = new ExportManager();
                }

                return _exportManager;
            }
            set
            {

            }
        }
        #endregion
    }
}
