﻿
namespace CustomerSearchInternal.BusinessLayer
{
    using CustomerSearchInternal.Enum;
    #region NAMESPACES
    using CustomerSearchInternal.Models;
    using CustomerSearchInternal.Models.Request;
    using System;
    using System.Collections.Generic;
    #endregion

    public class CustomerSearchBusinessManager : BusinessManagerBase
    {
        #region PUBLIC METHODS

        /// <summary>
        /// Method to get the customers that match search criteria
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public CustomerSearchResult GetCustomerMatches(SearchCriteria searchCriteria)
        {
            return CustomerSearchManager.GetCustomerMatches(searchCriteria);
        }

        /// <summary>
        /// Method to get a list of state codes
        /// </summary>
        /// <returns></returns>
        public List<StateCode> GetStates()
        {
            return CustomerSearchManager.GetStates();
        }

        /// <summary>
        /// Method to get the order history by customer
        /// </summary>
        /// <param name="bpcsCustomerNum"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Returns List<OrderHistory></returns>
        public List<OrderHistory> GetOrderHistory(string bpcsCustomerNum, DateTime startDate, DateTime endDate)
        {
            return new List<OrderHistory>(CustomerSearchManager.GetOrderHistory(bpcsCustomerNum, startDate, endDate));
        }

        /// <summary>
        /// Method to get the order detail
        /// </summary>
        /// <param name="bpcsCustomerNum"></param>
        /// <param name="invoiceNum"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Returns list of OrderDetail</returns>
        public List<OrderDetail> GetOrderDetail(string bpcsCustomerNum, string invoiceNum, DateTime startDate, DateTime endDate)
        {
            return CustomerSearchManager.GetOrderDetail(bpcsCustomerNum, invoiceNum, startDate, endDate);
        }

        /// <summary>
        /// Method to get a list of import files names
        /// </summary>
        /// <returns></returns>
        public List<string> GetFileNames()
        {
            return CustomerSearchManager.GetFileNames();
        }

        /// <summary>
        /// Method to insert the import file info
        /// </summary>
        /// <param name="importRequest"></param>
        /// <returns>Returns ImportRequest</returns>
        public ImportRequest InsertImportFileInfo(ImportRequest importRequest)
        {
            importRequest = CustomerSearchManager.InsertImportFileInfo(importRequest);
            return importRequest;
        }

        #endregion
    }
}
