﻿using CustomerSearchInternal.DataLayer;
using CustomerSearchInternal.Enum;
using CustomerSearchInternal.Interfaces;
using CustomerSearchInternal.Logging;
using CustomerSearchInternal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.BusinessLayer
{
    public class ExportBusinessManager : BusinessManagerBase, IExportManager
    {
        /// <summary>
        /// Method that calls to start the batch process
        /// </summary>
        public void StartBatchProcess()
        {
            ExportManager.StartBatchProcess();
        }

        /// <summary>
        /// Method that exports customer data with or without history
        /// </summary>
        /// <param name="customerList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="selectedRows"></param>
        /// <param name="isExportMode"></param>
        /// <param name="searchCriteria"></param>
        /// <param name="customerDataOnly"></param>
        public DataTable ExportData(string customerList, DateTime startDate, DateTime endDate, List<CustomerDetail> selectedRows, bool isExportMode, List<SearchCriteria> searchCriteria, bool customerDataOnly)
        {
            return ExportManager.ExportData(customerList, startDate, endDate, selectedRows, isExportMode, searchCriteria, customerDataOnly);
        }

        /// <summary>
        /// The export No Matched Search details
        /// </summary>
        /// <param name="noMatchCustomers"></param>
        /// <param name="isCustomerSearchMode"></param>
        /// <param name="importColumnCount"></param>
        public DataTable ExportDataForNomatch(List<SearchResult> noMatchCustomers)
        {
            return ExportManager.ExportDataForNomatch(noMatchCustomers);
        }
    }
}
