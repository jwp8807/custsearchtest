﻿namespace CustomerSearchInternal.Models
{
    #region NAMESPACES
    using System;
    using System.Collections.Generic;
    #endregion

    public class OrderHistory : ModelBase
    {
        #region Fields
        private DateTime _invoiceDate;
        private string _invoiceNumber;

        private DateTime _orderDate;

        private string _orderNumber = string.Empty;

        private string _externalOrderNumber = string.Empty;

        private string _promisedOrderNumber = string.Empty;

        private double _invoiceTotal;

        private double _totalFormsAmount;

        private string _commissionTotal = string.Empty;

        private string _twelveMonthsTotal = string.Empty;

        private string _thirtySixMonthsTotal = string.Empty;

        private double _totalMarginAmount;

        private string _shipToName = string.Empty;

        private string _shipToAddress1 = string.Empty;

        private string _shipToAddress2 = string.Empty;

        private string _shipToCity = string.Empty;

        private string _shipToState = string.Empty;

        private string _shipToZip = string.Empty;

        private List<OrderDetail> _orderDetails;

        private string _orderDistributorId;

        private bool _isHistoryData; //Added for Sprint4-Display 2014 Order History data

        private double _totalCommissionAmount; //= string.Empty; //Added for Sprint4-Display Commission data //Changed datatype to double as part of Sprint5 changes

        private double _totalFreightAmount;
        #endregion Fields

        #region Properties
        public string OrderDistributorId
        {
            get
            {
                _orderDistributorId = _orderDistributorId ?? string.Empty;

                return _orderDistributorId;
            }

            set
            {
                _orderDistributorId = value;
            }
        }

        public double InvoiceTotal
        {
            get
            {
                return _invoiceTotal;
            }

            set
            {
                _invoiceTotal = value;
            }
        }

        public string OrderNumber
        {
            get
            {
                return _orderNumber;
            }

            set
            {
                _orderNumber = value;
            }
        }

        public string ExternalOrderNumber
        {
            get
            {
                return _externalOrderNumber;
            }

            set
            {
                _externalOrderNumber = value;
            }
        }

        public string PromisedOrderNumber
        {
            get
            {
                return _promisedOrderNumber;
            }

            set
            {
                _promisedOrderNumber = value;
            }
        }

        public double TotalFormsAmount
        {
            get
            {
                return _totalFormsAmount;
            }

            set
            {
                _totalFormsAmount = value;
            }
        }

        public double TotalMarginAmount
        {
            get
            {
                return _totalMarginAmount;
            }

            set
            {
                _totalMarginAmount = value;
            }
        }

        public DateTime OrderDate
        {
            get
            {
                if (_orderDate == null)
                {
                    _orderDate = new DateTime();
                }

                return _orderDate;
            }

            set
            {
                _orderDate = value;
            }
        }

        public string OrderDateValue
        {
            get
            {
                return _orderDate != null && _orderDate > DateTime.MinValue ? _orderDate.ToShortDateString() : string.Empty;
            }
        }

        public string InvoiceNumber
        {
            get
            {
                return _invoiceNumber;
            }

            set
            {
                _invoiceNumber = value;
            }
        }

        public DateTime InvoiceDate
        {
            get
            {
                if (_invoiceDate == null)
                {
                    _invoiceDate = new DateTime();
                }

                return _invoiceDate;
            }

            set
            {
                _invoiceDate = value;
            }
        }

        public string InvoiceDateValue
        {
            get
            {
                return _invoiceDate != null && _invoiceDate > DateTime.MinValue ? _invoiceDate.ToShortDateString() : string.Empty;
            }
        }

        public string TwelveMonthsTotal
        {
            get
            {
                return _twelveMonthsTotal;
            }

            set
            {
                _twelveMonthsTotal = value;
            }
        }

        public string ThirtySixMonthsTotal
        {
            get
            {
                return _thirtySixMonthsTotal;
            }

            set
            {
                _thirtySixMonthsTotal = value;
            }
        }

        public string ShipToName
        {
            get
            {
                return _shipToName;
            }
            set
            {
                _shipToName = value;
            }
        }

        public string ShipToAddress1
        {
            get
            {
                return _shipToAddress1;
            }
            set
            {
                _shipToAddress1 = value;
            }
        }

        public string ShipToAddress2
        {
            get
            {
                return _shipToAddress2;
            }
            set
            {
                _shipToAddress2 = value;
            }
        }

        public string ShipToCity
        {
            get
            {
                return _shipToCity;
            }
            set
            {
                _shipToCity = value;
            }
        }

        public string ShipToState
        {
            get
            {
                return _shipToState;
            }
            set
            {
                _shipToState = value;
            }
        }

        public string ShipToZip
        {
            get
            {
                return _shipToZip;
            }
            set
            {
                _shipToZip = value;
            }
        }

        public List<OrderDetail> OrderDetails
        {
            get
            {
                if (_orderDetails == null)
                {
                    _orderDetails = new List<OrderDetail>();
                }

                return _orderDetails;
            }

            set
            {
                _orderDetails = value;
            }
        }

        //Added for Sprint4-Display 2014 Order History data
        public bool IsHistoryData
        {
            get { return _isHistoryData; }
            set
            {
                _isHistoryData = value;
            }
        }
        //Added for Sprint4-Display Commission data
        public double TotalCommissionAmount
        {
            get
            {
                return _totalCommissionAmount;
            }

            set
            {
                _totalCommissionAmount = value;
            }
        }

        //Added for SFG-181
        public double TotalFreightAmount
        {
            get
            {
                return _totalFreightAmount;
            }

            set
            {
                _totalFreightAmount = value;
            }
        }
        #endregion Properties
    }
}
