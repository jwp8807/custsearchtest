﻿using CustomerSearchInternal.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.Models
{
    public class CustomerDetail: ModelBase
    {
        #region FIELDS
        private string _bpcsCustomerNumber = string.Empty;
        private string _distributorID;
        private string _distributorName;
        private string _nameMatch;
        private string _addressMatch;
        private string _phoneMatch;
        private string _doNotRotate;
        private DateTime _lastOrderDate;
        private DateTime _lastInvoiceDate;
        private DateTime _firstOrderDate;
        private string _lastInvoiceDateValue = string.Empty;
        private string _externalCustomerNumber;
        private string _billToName = string.Empty;
        private string _billToAddress1 = string.Empty;
        private string _billToAddress2 = string.Empty;
        private string _billToCity = string.Empty;
        private string _billToState = string.Empty;
        private string _billToZip = string.Empty;
        private string _billToPhone = string.Empty;
        private string _shipToName = string.Empty;
        private string _shipToAddress1 = string.Empty;
        private string _shipToAddress2 = string.Empty;
        private string _shipToCity = string.Empty;
        private string _shipToState = string.Empty;
        private string _shipToZip = string.Empty;
        private string _customerStatus = string.Empty;
        private bool _isChecked = false;
        private bool _isAllChecked = false;
        private string _referenceNumber = string.Empty;
        private string _distributorPhoneNumber = string.Empty;
        private string _distributorFaxNumber = string.Empty;
        private string _distributorEmailAddress = string.Empty;
        private SearchCriteria _searchCriteria;
        private List<OrderHistory> _orderHistory;
        private List<OrderDetail> _orderDetails;
        #endregion Fields

        #region PROPERTIES
        public ExportType ExportType { get; set; }

        public List<OrderHistory> OrderHistory
        {
            get
            {
                if (_orderHistory == null)
                {
                    _orderHistory = new List<OrderHistory>();
                }

                return _orderHistory;
            }

            set
            {
                _orderHistory = value;
            }
        }

        public List<OrderDetail> OrderDetails
        {
            get
            {
                if (_orderDetails == null)
                {
                    _orderDetails = new List<OrderDetail>();
                }

                return _orderDetails;
            }

            set
            {
                _orderDetails = value;
            }
        }

        public SearchCriteria SearchCriteria
        {
            get
            {
                if(_searchCriteria == null)
                {
                    _searchCriteria = new SearchCriteria();
                }

                return _searchCriteria;
            }
            set
            {
                _searchCriteria = value;
            }
        }

        public string BPCSCustomerNumber
        {
            get
            {
                return _bpcsCustomerNumber;
            }
            set
            {
                _bpcsCustomerNumber = value;
            }
        }

        public string FileName { get; set; }

        public bool ExcludeDistributors { get; set; }

        public string DistributorID { get; set; }

        public string DistributorName { get; set; }

        public string NameMatch { get; set; }

        public string AddressMatch { get; set; }

        public string PhoneMatch { get; set; }

        public string DoNotRotate { get; set; }

        public string CustomerStatus { get; set; }

        public DateTime LastOrderDate { get; set; }

        public DateTime LastInvoiceDate { get; set; }

        public DateTime FirstOrderDate { get; set; }

        public string FirstOrderDateValue
        {
            get
            {
                return _firstOrderDate != null && _firstOrderDate > DateTime.MinValue ? _firstOrderDate.ToShortDateString() : string.Empty;
            }
        }

        public string LastInvoiceDateValue
        {
            get
            {
                return _lastInvoiceDate != null && _lastInvoiceDate > DateTime.MinValue ? _lastInvoiceDate.ToShortDateString() : string.Empty;
            }
        }

        public string LastOrderDateValue
        {
            get
            {
                return _lastOrderDate != null && _lastOrderDate > DateTime.MinValue ? _lastOrderDate.ToShortDateString() : string.Empty;
            }
        }

        public string ExternalCustomerNumber { get; set; }

        public string BillToName { get; set; }

        public string BillToAddress1 { get; set; }

        public string BillToAddress2 { get; set; }

        public string BillToCity { get; set; }

        public string BillToState { get; set; }

        public string BillToZip { get; set; }

        public string BillToPhone { get; set; }

        public string ShipToName { get; set; }

        public string ShipToAddress1 { get; set; }

        public string ShipToAddress2 { get; set; }

        public string ShipToCity { get; set; }

        public string ShipToState { get; set; }

        public string ShipToZip { get; set; }

        public bool IsChecked { get; set; }

        public string ReferenceNumber { get; set; }

        public string DistributorPhoneNumber { get; set; }

        public string DistributorFaxNumber { get; set; }

        public string DistributorEmailAddress { get; set; }

        public string InvoiceNumber { get; set; }

        #endregion Properties
    }
}
