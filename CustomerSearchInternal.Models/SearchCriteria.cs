﻿namespace CustomerSearchInternal.Models
{
    using System;
    #region NAMESPACES
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Text.RegularExpressions;
    #endregion

    public class SearchCriteria : ModelBase
    {
        #region FIELDS
        private string _bpcsCustomerNumber = string.Empty;
        private string _name;
        private string _address;
        private string _city;
        private string _state;
        private string _zip;
        private string _phone;
        private string _distributorId;
        private string _customerId;
        private string _externalCustomerId;
        private string _referenceNumber;
        private string _address1;
        private string _address2;
        private string _errorMatch;
        private string _standardizedAddress;
        private string _standardizedZip;
        private string _perMailError;
        private string _distributorIdForExport;
        private string _userId;
        private List<StateCode> _states;
        private DateTime _startDate, _endDate;
        #endregion Fields

        #region PROPERTIES
        public bool IsAproSearch { get; set; }
        public bool IsCustomerSearch { get; set; }
        public List<StateCode> States
        {
            get
            {
                if (_states == null)
                {
                    _states = new List<StateCode>();
                }

                return _states;
            }
            set
            {
                _states = value;
            }
        }

        public string DistributorId
        {
            get
            {
                return _distributorId;
            }
            set
            {
                _distributorId = value;
            }
        }

        public string CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        public string ExternalCustomerId
        {
            get
            {
                return _externalCustomerId;
            }
            set
            {
                _externalCustomerId = value;
            }
        }

        public string UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }
        public string BPCSCustomerNumber
        {
            get
            {
                return _bpcsCustomerNumber;
            }
            set
            {
                _bpcsCustomerNumber = value;
            }
        }

        [Required(ErrorMessage = "Name field is required")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        [Required(ErrorMessage = "Address field is required")]
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
            }
        }

        public string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }

        public string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }

        [Required(ErrorMessage = "City field is required")]
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
            }
        }

        [Required]
        [DisplayName("State / Province")]
        [StringLength(2, ErrorMessage = "State / Province field is required")]
        public string State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        [Required(ErrorMessage = "Zip / Postal Code field is required")]
        public string Zip
        {
            get
            {
                return _zip;
            }
            set
            {
                _zip = value;
            }
        }

        [Required(ErrorMessage = "Phone field is required")]
        [RegularExpression(@"^(\+\s?)?((?<!\+.*)\(\+?\d+([\s\-\.]?\d+)?\)|\d+)([\s\-\.]?(\(\d+([\s\-\.]?\d+)?\)|\d+))*(\s?(x|ext\.?)\s?\d+)?$", ErrorMessage = "Invalid Phone Number")]
        public string Phone
        {
            get
            {
                if (_phone != null)
                {
                    _phone = Regex.Replace(_phone, "[^0-9.]", "");
                }
                return _phone;
            }
            set
            {
                _phone = value;
            }
        }

        public string ReferenceNumber
        {
            get
            {
                return _referenceNumber;
            }
            set
            {
                _referenceNumber = value;
            }
        }

        public string CriteriaString
        {
            set { }
            get
            {
                List<string> allCriterias = new List<string>() {    this.ReferenceNumber,
                                                                    this.Name,
                                                                    this.Address1,
                                                                    this.Address2,
                                                                    this.City,
                                                                    this.State,
                                                                    this.Zip,
                                                                    this.Phone,
                                                                    this.CustomerId,
                                                                    this.ExternalCustomerId,
                                                                    this.StandardizedAddress,
                                                                    this.StandardizedZip,
                                                                    this.PerMailError }; //Added Cust#,ExtCust# as part of ALM 2436

                if (allCriterias.Count >= 12)
                {
                    if (allCriterias[10] != null)
                    {
                        if (!string.IsNullOrEmpty(allCriterias[10].ToString()))
                        {
                            allCriterias[10] = "        STD ADDRESS LINE: " + this.StandardizedAddress;
                        }
                    }
                    if (allCriterias[11] != null)
                    {
                        if (!string.IsNullOrEmpty(allCriterias[11].ToString()))
                        {
                            allCriterias[11] = "   STD ZIP: " + this.StandardizedZip;
                        }
                    }
                    if (allCriterias[12] != null)
                    {
                        if (!string.IsNullOrEmpty(allCriterias[12].ToString()))
                        {
                            allCriterias[12] = "   PERMAIL ERROR INDICATOR: " + this.PerMailError;
                        }
                    }
                }

                allCriterias.RemoveAll(x => string.IsNullOrWhiteSpace(x));

                return allCriterias.Count > 0 ? string.Join(", ", allCriterias) : string.Empty;
            }
        }
        //Added ErrorMatch property for Safeguard project ALM #2392  to export Customer With NoMatch
        public string ErrorMatch
        {
            get { return _errorMatch; }
            set
            {
                _errorMatch = value;
            }
        }

        public string StandardizedAddress
        {
            get
            {
                return _standardizedAddress;
            }
            set
            {
                _standardizedAddress = value;
            }
        }

        public string StandardizedZip
        {
            get
            {
                return _standardizedZip;
            }
            set
            {
                _standardizedZip = value;
            }
        }

        public string PerMailError
        {
            get
            {
                return _perMailError;
            }
            set
            {
                _perMailError = value;
            }
        }

        public string DistributorIdForExport
        {
            get
            {
                return _distributorIdForExport;
            }
            set
            {
                _distributorIdForExport = value;
            }
        }
        #endregion Properties
    }
}
