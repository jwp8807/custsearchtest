﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.Models
{
    public class CustomerSearchResult
    {
        #region FIELDS
        private List<CustomerDetail> _customers;
        private ResultCode _result;
        private string _searchCount;
        private string _standardizedAddressLine = string.Empty;
        private string _standardizedZip = string.Empty;
        private string _perMailError = string.Empty;
        private SearchCriteria _searchCriteria;
        #endregion

        #region PROPERTIES
        public bool WasSearchPerformed { get; set; }
        public string ReferenceNumber { get; set; }

        public List<CustomerDetail> Customers
        {
            get
            {
                if (_customers == null)
                {
                    _customers = new List<CustomerDetail>();
                }

                return _customers;
            }
            set
            {
                if (value != null)
                {
                    _customers = value;
                }
            }
        }

        public ResultCode Result
        {
            get
            {
                if (_result == null)
                {
                    _result = new ResultCode();
                }

                return _result;
            }

            set
            {
                _result = value;
            }
        }

        public string SearchCount
        {
            get { return _searchCount; }
            set
            {
                _searchCount = value;
            }
        }

        public string StandardizedAddressLine
        {
            get { return _standardizedAddressLine; }
            set
            {
                _standardizedAddressLine = value;
            }
        }

        public string StandardizedZip
        {
            get { return _standardizedZip; }
            set
            {
                _standardizedZip = value;
            }
        }

        public string PerMailError
        {
            get { return _perMailError; }
            set
            {
                _perMailError = value;
            }
        }

        public bool IsSuccessful { get; set; }

        public SearchCriteria SearchCriteria
        {
            get
            {
                if (_searchCriteria == null)
                {
                    _searchCriteria = new SearchCriteria();
                }

                return _searchCriteria;
            }

            set
            {
                _searchCriteria = value;
            }
        }
        #endregion
    }
}
