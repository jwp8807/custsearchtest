﻿namespace CustomerSearchInternal.Models
{
    public class ExportSelectedRow
    {
        public string ExportType { get; set; }
        public string DistId { get; set; }
        public string DistName { get; set; }
        public string DistPhone { get; set; }
        public string DistFax { get; set; }
        public string DistEmail { get; set; }
        public string BillName { get; set; }
        public string BillAddr1 { get; set; }
        public string BillAddr2 { get; set; }
        public string BillCity { get; set; }
        public string BillState { get; set; }
        public string BillZip { get; set; }
        public string BillPhone { get; set; }
        public string ShipName { get; set; }
        public string ShipAddr1 { get; set; }
        public string ShipAddr2 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipZip { get; set; }
        public string FirstOrderDate { get; set; }
        public string LastOrderDate { get; set; }
        public string LastInvoiceDate { get; set; }
        public string CustomerStatus { get; set; }
        public string BpcsCustNum { get; set; }
        public string ExternalCustNum { get; set; }
    }
}
