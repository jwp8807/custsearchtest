﻿namespace CustomerSearchInternal.Models
{
    #region NAMESPACES
    using CustomerSearchInternal.Enum;
    using System.Collections.Generic;
    #endregion

    public class SearchResult : ModelBase
    {
        #region Fields
        private SearchCriteria _criteria;

        private List<CustomerDetail> _customers;

        private bool _isAllChecked;

        private MessageType _type;
        #endregion Fields

        #region Properties
        public bool IsAllChecked
        {
            get
            {
                return _isAllChecked;
            }
            set
            {
                CheckUncheckCustomers(value);
                _isAllChecked = value;
            }
        }

        public List<CustomerDetail> Customers
        {
            get
            {
                if (_customers == null)
                {
                    _customers = new List<CustomerDetail>();
                }

                return _customers;
            }
            set
            {
                if (value != null)
                {
                    _customers = value;
                }
            }
        }

        public SearchCriteria Criteria
        {
            get
            {
                if (_criteria == null)
                {
                    _criteria = new SearchCriteria();
                }

                return _criteria;
            }

            set
            {
                _criteria = value;
            }
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public MessageType Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }
        #endregion Properties

        #region Methods
        public void CheckUncheckCustomers(bool value)
        {
            foreach (CustomerDetail c in Customers)
            {
                c.IsChecked = value;
            }
        }
        #endregion Methods
    }
}
