﻿namespace CustomerSearchInternal.Models
{
    public class OrderDetail : ModelBase
    {
        #region Fields
        private string _orderNumber = string.Empty;

        private string _purchaseOrderNumber = string.Empty;

        private string _productCode = string.Empty;

        private string _productDescription = string.Empty;

        private string _quantityPerProductCode = string.Empty;

        private string _shipToName = string.Empty;

        private string _shipToAddress = string.Empty;

        private string _shipToCity = string.Empty;

        private string _shipToState = string.Empty;

        private string _shipToZip = string.Empty;

        private string _invoiceTotal = string.Empty;

        private string _quantity = string.Empty;

        private double _formsAmount;//= string.Empty; //Changed datatype from string to double as part of sprint5 change COARP172

        private double _marginAmount;//= string.Empty; //Changed datatype from string to double as part of sprint5 change COARP172

        private string _lineItemNumber = string.Empty;

        private bool _isHistoryData; //Added for Sprint4-Display 2014 Order History data

        private string _transactionType = string.Empty;

        private double _commissionAmount;

        private string _productLine = string.Empty;

        private string _vendorID = string.Empty;

        private string _vendorName = string.Empty;

        private double _freightAmount;
        #endregion Fields

        #region Properties
        public string LineItemNumber
        {
            get
            {
                return _lineItemNumber;
            }
            set
            {
                _lineItemNumber = value;
            }
        }

        public double FormsAmount
        {
            get
            {
                return _formsAmount;
            }
            set
            {
                _formsAmount = value;
            }
        }

        public double MarginAmount
        {
            get
            {
                return _marginAmount;
            }
            set
            {
                _marginAmount = value;
            }
        }

        public string ShipToName
        {
            get
            {
                return _shipToName;
            }
            set
            {
                _shipToName = value;
            }
        }

        public string ShipToAddress
        {
            get
            {
                return _shipToAddress;
            }
            set
            {
                _shipToAddress = value;
            }
        }

        public string ShipToCity
        {
            get
            {
                return _shipToCity;
            }
            set
            {
                _shipToCity = value;
            }
        }

        public string ShipToState
        {
            get
            {
                return _shipToState;
            }
            set
            {
                _shipToState = value;
            }
        }

        public string ShipToZip
        {
            get
            {
                return _shipToZip;
            }
            set
            {
                _shipToZip = value;
            }
        }

        public string OrderNumber
        {
            get
            {
                return _orderNumber;
            }

            set
            {
                _orderNumber = value;
            }
        }

        public string PurchaseOrderNumber
        {
            get
            {
                return _purchaseOrderNumber;
            }

            set
            {
                _purchaseOrderNumber = value;
            }
        }

        public string ProductCode
        {
            get
            {
                return _productCode;
            }

            set
            {
                _productCode = value;
            }
        }

        public string ProductDescription
        {
            get
            {
                return _productDescription;
            }

            set
            {
                _productDescription = value;
            }
        }

        public string QuantityPerProductCode
        {
            get
            {
                return _quantityPerProductCode;
            }

            set
            {
                _quantityPerProductCode = value;
            }
        }

        public string Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
            }
        }

        public string InvoiceTotal
        {
            get
            {
                return _invoiceTotal;
            }

            set
            {
                _invoiceTotal = value;
            }
        }
        //sprint4
        //Added for Sprint4-Display 2014 Order History data
        public bool IsHistoryData
        {
            get { return _isHistoryData; }
            set
            {
                _isHistoryData = value;
            }
        }
        //Added for Sprint4-Display Commission data
        public string TransactionType
        {
            get
            {
                return _transactionType;
            }

            set
            {
                _transactionType = value;
            }
        }
        public double CommissionAmount
        {
            get
            {
                return _commissionAmount;
            }

            set
            {
                _commissionAmount = value;
            }
        }
        public string ProductLine
        {
            get
            {
                return _productLine;
            }
            set
            {
                _productLine = value;
            }
        }
        public string VendorID
        {
            get
            {
                return _vendorID;
            }
            set
            {
                _vendorID = value;
            }
        }
        public string VendorName
        {
            get
            {
                return _vendorName;
            }
            set
            {
                _vendorName = value;
            }
        }

        //Added for SFG-181
        public double FreightAmount
        {
            get
            {
                return _freightAmount;
            }
            set
            {
                _freightAmount = value;
            }
        }
        #endregion Properties
    }
}
