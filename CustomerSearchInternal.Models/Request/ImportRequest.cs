﻿namespace CustomerSearchInternal.Models.Request
{
    #region NAMESPACES
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    #endregion

    public class ImportRequest : ModelBase
    {
        #region FIELDS
        private string _emailAddress;
        private static string _fileName;
        private string _jobID;
        private string _userID;
        private string _shouldExcludeHistory;
        private string _shouldExcludeDistributor;
        private string _distributorsToExclude;
        public static string JobStatus = "Ready";
        private static int fileNameCount = 0;
        #endregion

        #region PROPERTIES
        public int WorksheetRowCount { get; set; }

        /// <summary>
        /// The File to Import
        /// </summary>
        [Required]
        public HttpPostedFileBase File { get; set; }

        /// <summary>
        /// Gets or sets the mail id
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                _emailAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the file name
        /// </summary>
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
            }
        }

        /// <summary>
        /// Gets or sets the exclude history
        /// </summary>
        public string ExcludeHistory
        {
            get { return _shouldExcludeHistory; }
            set
            {
                _shouldExcludeHistory = value;
            }
        }

        /// <summary>
        /// Gets or sets the exclude distributor
        /// </summary>
        public string ExcludeDistributor
        {
            get { return _shouldExcludeDistributor; }
            set
            {
                _shouldExcludeDistributor = value;
            }
        }

        /// <summary>
        /// The distributors to exclude for the import request
        /// </summary>
        public string DistributorsToExlude
        {
            get { return _distributorsToExclude; }
            set
            {
                _distributorsToExclude = value;
            }
        }

        /// <summary>
        /// Gets or sets the job id
        /// </summary>
        public string JobID
        {
            get { return _jobID; }
            set
            {
                _jobID = value;
            }
        }

        /// <summary>
        /// Gets or sets the user id
        /// </summary>
        public string UserID
        {
            get { return _userID; }
            set
            {
                _userID = value;
            }
        }

        /// <summary>
        /// Gets or sets the file name count
        /// </summary>
        public static int FileNameCount
        {
            get { return ImportRequest.fileNameCount; }
            set { ImportRequest.fileNameCount = value; }
        }
        #endregion
    }
}
