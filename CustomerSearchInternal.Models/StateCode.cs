﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.Models
{
    public class StateCode
    {
        #region Fields
        private string _code = string.Empty;

        private string _description = string.Empty;
        #endregion Fields

        #region Properties
        public string Code
        {
            get
            {
                _code = _code ?? string.Empty;

                return _code;
            }

            set
            {
                _code = value;
            }
        }

        public string Name
        {
            get
            {
                _description = _description ?? string.Empty;

                return _description;
            }

            set
            {
                _description = value;
            }
        }
        #endregion Properties
    }
}
