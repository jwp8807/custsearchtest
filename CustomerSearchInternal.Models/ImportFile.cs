﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.Models
{
    public class ImportFile
    {
        public bool IsCopySuccessful { get; set; }
        public string SafeFileName { get; set; }
    }
}
