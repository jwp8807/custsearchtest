﻿namespace CustomerSearchInternal.Models
{
    #region NAMESPACES
    using System;
    #endregion

    public class ModelBase
    {
        #region Fields
        private string _scannedItem = string.Empty;

        private DateTime _startDate;

        private DateTime _endDate;

        private string _bpcsCustomerNumber = string.Empty;
        #endregion Fields

        #region Properties
        public string BPCSCustomerNumber
        {
            get
            {
                return _bpcsCustomerNumber;
            }
            set
            {
                _bpcsCustomerNumber = value;
            }
        }

        public string ScannedItem
        {
            get
            {
                return _scannedItem;
            }

            set
            {
                //// Convert always to upper case.
                value = value ?? string.Empty;
                _scannedItem = value.Trim().ToUpper();
            }
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                if (_startDate == null || _startDate == DateTime.MinValue)
                {
                    _startDate = DateTime.Now.AddMonths(-36);
                }

                return _startDate;
            }

            set
            {
                _startDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate
        {
            get
            {
                if (_endDate == null || _endDate == DateTime.MinValue)
                {
                    _endDate = DateTime.Now;
                }

                return _endDate;
            }

            set
            {
                _endDate = value;
            }
        }
        #endregion Properties
    }
}
