﻿namespace CustomerSearchInternal.Models
{
    #region NAMESPACES
    using System.Collections.Generic;
    #endregion

    public class ImportResult : ModelBase
    {
        #region FIELDS
        private List<SearchResult> _results;

        private List<SearchResult> _noMatchResults;
        private string _jobID;
        private SearchCriteria _searchCriteria;
        #endregion FIELDS

        #region PROPERTIES
        public bool IsImportSuccessful { get; set; }
        public string FileFormattingMessage { get; set; }
        public string FileRenameMessage { get; set; }
        public string SaveFileMessage { get; set; }
        public string SuccessfulResultMessage { get; set; }
        public string FileNotAccessibleMessage { get; set; }


        public SearchCriteria SearchCriteria
        {
            get
            {
                if (_searchCriteria == null)
                {
                    _searchCriteria = new SearchCriteria();
                }

                return _searchCriteria;
            }

            set
            {
                _searchCriteria = value;
            }
        }

        /// <summary>
        /// Gets or sets the job id
        /// </summary>
        public string JobID
        {
            get { return _jobID; }
            set
            {
                _jobID = value;
            }
        }

        public List<SearchResult> NoMatchResults
        {
            get
            {
                if (_noMatchResults == null)
                {
                    _noMatchResults = new List<SearchResult>();
                }

                return _noMatchResults;
            }

            set
            {
                _noMatchResults = value;
            }
        }

        public List<SearchResult> Results
        {
            get
            {
                if (_results == null)
                {
                    _results = new List<SearchResult>();
                }

                return _results;
            }

            set
            {
                _results = value;
            }
        }
        #endregion PROPERTIES
    }
}
