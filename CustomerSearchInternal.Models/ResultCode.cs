﻿namespace CustomerSearchInternal.Models
{
    public class ResultCode
    {
        #region Fields
        private string _code = string.Empty;
        private string _message = string.Empty;
        #endregion Fields

        #region Properties
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code
        {
            get
            {
                _code = _code ?? string.Empty;

                return _code;
            }

            set
            {
                _code = value;
            }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message
        {
            get
            {
                _message = _message ?? string.Empty;

                return _message;
            }

            set
            {
                _message = value;
            }
        }
        #endregion Properties
    }
}
