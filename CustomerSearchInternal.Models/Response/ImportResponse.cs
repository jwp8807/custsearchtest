﻿namespace CustomerSearchInternal.Models.Response
{
    #region NAMESPACES
    using CustomerSearchInternal.Models.Request;
    #endregion

    public class ImportResponse : ImportRequest
    {
        public string FileName { get; set; }
    }
}
