﻿namespace CustomerSearchInternal.DataLayer
{
    #region NAMESPACES
    using CustomerSearchInternal.DataLayer.CustomerSearchService;
    using CustomerSearchInternal.Interfaces;
    using CustomerSearchInternal.Logging;
    using CustomerSearchInternal.Models;
    using CustomerSearchInternal.Models.Request;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Deployment.Application;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    #endregion

    public class CustomerSearchDataManager : DataManagerBase, ICustomerSearchManager
    {
        #region PUBLIC METHODS
        /// <summary>
        /// Method that calls the CustomerSearchService to get the customer matches
        /// </summary>
        /// <param name="searchCriteria">todo: describe searchCriteria parameter on GetCustomerMatches</param>
        public CustomerSearchResult GetCustomerMatches(Models.SearchCriteria searchCriteria)
        {
            bool isSuccessful = false;
            CustomerSearchResult customerSearchResult = new CustomerSearchResult();
            try
            {
                if (searchCriteria != null)
                {
                    //Get Customer Matches from CustomerSearchService
                    CustomerSearchRequest request = CreateSearchRequest(searchCriteria);
                    CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                    CustomerSearchResponse response = customerSearchServiceManager.GetData(request);
                    customerSearchResult = PopulateCustomerSearchResult(response, searchCriteria);
                    customerSearchResult.IsSuccessful = isSuccessful;
                    customerSearchResult.SearchCriteria = searchCriteria;
                }
            }
            catch (Exception exception)
            {
                //Log exception.
                Logger.Error(exception.ToString());

                customerSearchResult.Result.Code = "00";
                customerSearchResult.Result.Message = "Invalid response from Customer Search Service.";
                customerSearchResult.IsSuccessful = false;
            }

            return customerSearchResult;
        }

        /// <summary>
        /// Method to get a list of states
        /// </summary>
        /// <returns></returns>
        public List<StateCode> GetStates()
        {
            List<StateCode> states = new List<StateCode>();
            try
            {
                CustomerSearchRequest request = new CustomerSearchRequest
                {
                    CustomerSearch = new CustomerSearch
                    {
                        ActionType = ActionType.States
                    },
                    Source = CreateSourceRequest("GetStates")
                };
                CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                CustomerSearchResponse response = customerSearchServiceManager.GetData(request);

                if (response != null)
                {
                    states = response.States.ToList().ConvertAll(x => new StateCode
                    {
                        Code = x.Code,
                        Name = x.Name
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.ToString());
            }

            return states;
        }

        /// <summary>
        /// Method to get order history by BPCS customer number
        /// </summary>
        /// <param name="bpcsCustomerNum"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>Returns list of OrderHistory</returns>
        public List<Models.OrderHistory> GetOrderHistory(string bpcsCustomerNum, DateTime startDate, DateTime endDate)
        {
            List<Models.OrderHistory> orderHistory = new List<Models.OrderHistory>();
            try
            {
                if (!string.IsNullOrWhiteSpace(bpcsCustomerNum))
                {
                    CustomerSearchRequest request = new CustomerSearchRequest
                    {
                        Source = CreateSourceRequest("OrderHistory"),
                        CustomerSearch = new CustomerSearch
                        {
                            ActionType = ActionType.OrderHistory,
                            CustomerDetail = new CustomerSearchService.CustomerDetail
                            {
                                BPCSCustomerNumber = bpcsCustomerNum
                            },
                            StartDate = startDate,
                            EndDate = endDate
                        }
                    };

                    CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                    CustomerSearchResponse response = customerSearchServiceManager.GetData(request);

                    if (response != null)
                    {
                        foreach (CustomerSearchService.OrderHistory row in response.OrderHistories)
                        {
                            orderHistory.Add(new Models.OrderHistory
                            {
                                BPCSCustomerNumber = row.BPCSCustomerNumber,
                                EndDate = row.EndDate,
                                StartDate = row.StartDate,
                                ExternalOrderNumber = row.ExternalOrderNumber,
                                InvoiceDate = row.InvoiceDate,
                                InvoiceNumber = row.InvoiceNumber,
                                InvoiceTotal = row.InvoiceTotal,
                                IsHistoryData = row.IsHistoryData,
                                OrderDate = row.OrderDate,
                                OrderDistributorId = row.OrderDistributorId,
                                OrderNumber = row.OrderNumber,
                                PromisedOrderNumber = row.PromisedOrderNumber,
                                ScannedItem = row.ScannedItem,
                                ShipToAddress1 = row.ShipToAddress1,
                                ShipToAddress2 = row.ShipToAddress2,
                                ShipToCity = row.ShipToCity,
                                ShipToName = row.ShipToName,
                                ShipToState = row.ShipToState,
                                ShipToZip = row.ShipToZip,
                                TotalCommissionAmount = row.TotalCommissionAmount,
                                TotalFormsAmount = row.TotalFormsAmount,
                                TotalFreightAmount = row.TotalFreightAmount,
                                TotalMarginAmount = row.TotalMarginAmount,
                                TwelveMonthsTotal = row.TwelveMonthsTotal
                            });
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at GetCustomerHistory() " + exception);
            }

            return orderHistory;
        }

        /// <summary>
        /// Method that gets the order details
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="bpcsCustomerNum">todo: describe bpcsCustomerNum parameter on GetOrderDetail</param>
        /// <param name="invoiceNum">todo: describe invoiceNum parameter on GetOrderDetail</param>
        /// <returns>Returns IEnumerable of OrderDetailViewModel</returns>
        public List<Models.OrderDetail> GetOrderDetail(string bpcsCustomerNum, string invoiceNum, DateTime startDate, DateTime endDate)
        {
            List<Models.OrderDetail> orderDetails = new List<Models.OrderDetail>();
            try
            {
                if (!string.IsNullOrEmpty(bpcsCustomerNum) && !string.IsNullOrEmpty(invoiceNum))
                {
                    CustomerSearchServiceClient client = new CustomerSearchServiceClient();
                    CustomerSearchRequest request = new CustomerSearchRequest
                    {
                        Source = CreateSourceRequest("OrderDetails"),
                        CustomerSearch = new CustomerSearch
                        {
                            ActionType = ActionType.OrderDetails,
                            CustomerDetail = new CustomerSearchService.CustomerDetail
                            {
                                BPCSCustomerNumber = bpcsCustomerNum,
                                InvoiceNumber = invoiceNum
                            },
                            StartDate = startDate,
                            EndDate = endDate
                        }
                    };

                    CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                    CustomerSearchResponse response = customerSearchServiceManager.GetData(request);

                    if (response != null)
                    {
                        foreach (CustomerSearchService.OrderDetail row in response.OrderDetails)
                        {
                            orderDetails.Add(new Models.OrderDetail
                            {
                                BPCSCustomerNumber = row.BPCSCustomerNumber,
                                CommissionAmount = row.CommissionAmount,
                                EndDate = row.EndDate,
                                FormsAmount = row.FormsAmount,
                                FreightAmount = row.FreightAmount,
                                InvoiceTotal = row.InvoiceTotal,
                                IsHistoryData = row.IsHistoryData,
                                LineItemNumber = row.LineItemNumber,
                                MarginAmount = row.MarginAmount,
                                OrderNumber = row.OrderNumber,
                                ProductCode = row.ProductCode,
                                ProductDescription = row.ProductDescription,
                                ProductLine = row.ProductLine,
                                PurchaseOrderNumber = row.PurchaseOrderNumber,
                                Quantity = row.Quantity,
                                QuantityPerProductCode = row.QuantityPerProductCode,
                                ScannedItem = row.ScannedItem,
                                ShipToAddress = row.ShipToAddress,
                                ShipToCity = row.ShipToCity,
                                ShipToName = row.ShipToName,
                                ShipToState = row.ShipToState,
                                ShipToZip = row.ShipToZip,
                                StartDate = row.StartDate,
                                TransactionType = row.TransactionType,
                                VendorID = row.VendorID,
                                VendorName = row.VendorName
                            });
                        }
                    }

                    client.Close();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at GetOrderDetails() " + exception);
            }

            return orderDetails;
        }

        /// <summary>
        /// This method is used to fetch filenames
        /// for eradicating duplicate file creation. ALM 2393
        /// </summary>
        /// <returns>Distinct file names</returns>
        public List<string> GetFileNames()
        {
            List<string> fileNames = new List<string>();
            try
            {
                CustomerSearchRequest request = new CustomerSearchRequest
                {
                    Source = CreateSourceRequest("File Names"),
                    CustomerSearch = new CustomerSearch
                    {
                        ActionType = ActionType.FileNames
                    }
                };
                CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                CustomerSearchResponse response = customerSearchServiceManager.GetData(request);

                if (response != null && response.FileNames != null)
                {
                    fileNames = response.FileNames.ToList();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at GetFileNames() " + exception);
            }

            return fileNames;
        }

        /// <summary>
        /// Method to insert the import request info into database for batch processing
        /// </summary>
        /// <param name="importRequest"></param>
        /// <returns>Returns the ImportRequest object</returns>
        public ImportRequest InsertImportFileInfo(ImportRequest importRequest)
        {
            try
            {
                CustomerSearchRequest request = new CustomerSearchRequest
                {
                    Source = CreateSourceRequest("Insert Customer Info"),
                    CustomerSearch = new CustomerSearch
                    {
                        CustomerInfo = new CustomerInfo
                        {
                            BPCSCustomerNumber = importRequest.BPCSCustomerNumber,
                            EndDate = importRequest.EndDate,
                            ExcludeDistributor = importRequest.DistributorsToExlude,
                            ExcludeHistory = importRequest.ExcludeHistory,
                            FileName = importRequest.FileName,
                            JobID = importRequest.JobID,
                            MailID = importRequest.EmailAddress,
                            ScannedItem = importRequest.ScannedItem,
                            StartDate = importRequest.StartDate,
                            UserID = importRequest.UserID
                        }
                    }
                };

                CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                CustomerSearchResponse response = customerSearchServiceManager.InsertData(request);

                if (response != null && response.CustomerInfo != null)
                {
                    importRequest = new ImportRequest
                    {
                        BPCSCustomerNumber = response.CustomerInfo.BPCSCustomerNumber,
                        EndDate = response.CustomerInfo.EndDate,
                        DistributorsToExlude = response.CustomerInfo.ExcludeDistributor,
                        ExcludeHistory = response.CustomerInfo.ExcludeHistory,
                        FileName = response.CustomerInfo.FileName,
                        JobID = response.CustomerInfo.JobID,
                        EmailAddress = response.CustomerInfo.MailID,
                        ScannedItem = response.CustomerInfo.ScannedItem,
                        StartDate = response.CustomerInfo.StartDate,
                        UserID = response.CustomerInfo.UserID
                    };
                }

                return importRequest;
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at GetFileNames() " + exception);
                return importRequest;
            }
        }

        #endregion

        #region PRIVATE METHODS
        /// <summary>
        /// Method to create the request object for the Customer Search Service
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns>Returns CustomerSearchRequest object</returns>
        private static CustomerSearchRequest CreateSearchRequest(Models.SearchCriteria searchCriteria)
        {
            CustomerSearchRequest request = new CustomerSearchRequest();
            if (searchCriteria != null)
            {
                request.Source = CreateSourceRequest("CustomerSearch");
                request.CustomerSearch = new CustomerSearch
                {
                    ActionType = ActionType.CustomerSearch,
                    IsCustomerSearchMode = searchCriteria.IsCustomerSearch,
                    ShouldSearchForPhoneOnly = false,
                    SearchCriteria = new CustomerSearchService.SearchCriteria
                    {
                        Address1 = searchCriteria.Address1,
                        Address2 = searchCriteria.Address1,
                        BPCSCustomerNumber = searchCriteria.BPCSCustomerNumber,
                        CriteriaString = searchCriteria.CriteriaString,
                        DistributorAddress = searchCriteria.Address,
                        DistributorCity = searchCriteria.City,
                        DistributorId = searchCriteria.DistributorId,
                        DistributorName = searchCriteria.Name,
                        DistributorPhone = searchCriteria.Phone,
                        DistributorState = searchCriteria.State,
                        DistributorZip = searchCriteria.Zip,
                        EndDate = DateTime.Now,
                        StartDate = DateTime.Now.AddYears(-3)
                    }
                };
            }

            return request;
        }

        /// <summary>
        /// Create the source request used by the CustomerSearchService
        /// </summary>
        /// <param name="action"></param>
        /// <returns>Returns Source object</returns>
        private static Source CreateSourceRequest(string action)
        {
            string userId = string.Empty;
            if (HttpContext.Current != null)
            {
                userId = HttpContext.Current.User.Identity.Name;
            }
            return new Source
            {
                Action = action,
                ApplicationName = "Customer Search",
                ApplicationVersion = CurrentVersion,
                UserIdentity = userId
            };


        }

        /// <summary>
        /// Method to populate the MainViewModel object with the CustomerSearchResponse object data
        /// </summary>
        /// <param name="response"></param>
        /// <param name="viewModel"></param>
        /// <param name="searchCriteria">todo: describe searchCriteria parameter on PopulateViewModel</param>
        private static CustomerSearchResult PopulateCustomerSearchResult(CustomerSearchResponse response, Models.SearchCriteria searchCriteria)
        {
            CustomerSearchResult result = new CustomerSearchResult();

            //// Populate result code.
            if (response != null)
            {
                result.Result.Code = response.Result.Code;
                result.Result.Message = response.Result.Message;
                result.SearchCount = response.SearchCount; //Fetching total result count from SOA response and added as part of ALM #2375 


                result.StandardizedAddressLine = !string.IsNullOrWhiteSpace(response.StandardizedAddressLine) ? response.StandardizedAddressLine : string.Empty;
                result.StandardizedZip = !string.IsNullOrWhiteSpace(response.StandardizedZip) ? response.StandardizedZip : string.Empty;
                result.PerMailError = !string.IsNullOrWhiteSpace(response.PerMailError) ? response.PerMailError : string.Empty;
            }

            //// Check if response is present.
            if (response != null && response.Customers != null && response.Customers.Length > 0)
            {
                foreach (CustomerSearchService.CustomerDetail customer in response.Customers)
                {
                    Models.CustomerDetail customerDetail = new Models.CustomerDetail();
                    customerDetail.CustomerStatus = customer.CustomerStatus != null ? customer.CustomerStatus.ToString() : string.Empty;
                    customerDetail.DoNotRotate = customer.DoNotRotate.ToString();
                    customerDetail.ReferenceNumber = !string.IsNullOrWhiteSpace(customer.ReferenceNumber) ? customer.ReferenceNumber : string.Empty;
                    customerDetail.BPCSCustomerNumber = !string.IsNullOrWhiteSpace(customer.BPCSCustomerNumber) ? customer.BPCSCustomerNumber : string.Empty;
                    customerDetail.DistributorID = !string.IsNullOrWhiteSpace(customer.DistributorID) ? customer.DistributorID : string.Empty;
                    customerDetail.DistributorName = !string.IsNullOrWhiteSpace(customer.DistributorName) ? customer.DistributorName : string.Empty;
                    customerDetail.NameMatch = customer.NameMatch.ToString();
                    customerDetail.AddressMatch = customer.AddressMatch.ToString();
                    customerDetail.PhoneMatch = customer.PhoneMatch.ToString();
                    customerDetail.ShipToAddress1 = customer.ShipToAddress1;
                    customerDetail.ShipToAddress2 = customer.ShipToAddress2;
                    customerDetail.ShipToCity = customer.ShipToCity;
                    customerDetail.ShipToName = customer.ShipToName;
                    customerDetail.ShipToState = customer.ShipToState;
                    customerDetail.ShipToZip = customer.ShipToZip;
                    customerDetail.BillToPhone = customer.BillToPhone != null ? customer.BillToPhone : string.Empty;
                    customerDetail.BillToAddress1 = customer.BillToAddress1;
                    customerDetail.BillToAddress2 = customer.BillToAddress2;
                    customerDetail.BillToCity = customer.BillToCity;
                    customerDetail.BillToName = customer.BillToName;
                    customerDetail.BillToState = customer.BillToState;
                    customerDetail.BillToZip = customer.BillToZip;
                    customerDetail.LastOrderDate = customer.LastOrderDate;
                    customerDetail.CustomerStatus = customer.CustomerStatus;
                    customerDetail.SearchCriteria = searchCriteria;

                    result.Customers.Add(customerDetail);
                }
            }

            //Mark that a search was performed
            result.WasSearchPerformed = true;

            return result;
        }

        /// <summary>
        /// Gets the current version number of the Customer Search application
        /// </summary>
        private static string CurrentVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed
                       ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                       : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
        #endregion
    }
}
