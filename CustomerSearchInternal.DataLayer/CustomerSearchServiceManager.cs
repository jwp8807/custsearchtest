﻿using CustomerSearchInternal.DataLayer.CustomerSearchService;
using CustomerSearchInternal.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.DataLayer
{
    public class CustomerSearchServiceManager : DataManagerBase
    {
        #region PUBLIC METHODS

        /// <summary>
        /// Method that calls the CustomerSearchService GetData service
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns CustomerSearchResponse object</returns>
        public CustomerSearchResponse GetData(CustomerSearchRequest request)
        {
            CustomerSearchResponse response = new CustomerSearchResponse();
            try
            {
                Logger.Info("Starting service connect for GetData.");

                CustomerSearchServiceClient client = new CustomerSearchServiceClient();
                Logger.Info(string.Concat("Search Request for GetData:\n", SerializeObject<CustomerSearchRequest>(request)));

                response = client.GetData(request);
                if (request.CustomerSearch.ActionType != ActionType.States)
                {
                    Logger.Info(string.Concat("Search Response for GetData:\n", SerializeObject<CustomerSearchResponse>(response)));
                }

                client.Close();
            }
            catch (Exception exception)
            {
                Logger.Error("Exception occurred at GetData: \n" + exception);
            }

            return response;
        }

        /// <summary>
        /// Method that calls the CustomerSearchService InsertData service
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns CustomerSearchResponse object</returns>
        public CustomerSearchResponse InsertData(CustomerSearchRequest request)
        {
            CustomerSearchResponse response = new CustomerSearchResponse();
            try
            {
                Logger.Info("Starting service connect for InsertData.");
                CustomerSearchServiceClient client = new CustomerSearchServiceClient();
                Logger.Info(string.Concat("Search Request for InsertData:\n", SerializeObject<CustomerSearchRequest>(request)));

                response = client.InsertData(request);
                Logger.Info(string.Concat("Search Response for InsertData:\n", SerializeObject<CustomerSearchResponse>(response)));

                client.Close();
            }
            catch (Exception exception)
            {
                Logger.Error("Exception occurred at InsertData: \n" + exception);
            }

            return response;
        }

        /// <summary>
        /// Method that calls the CustomerSearchService ExportData service
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Returns CustomerSearchResponse object</returns>
        public CustomerSearchResponse ExportData(CustomerSearchRequest request)
        {
            CustomerSearchResponse response = new CustomerSearchResponse();
            try
            {
                Logger.Info("Starting service connect for ExportData.");

                CustomerSearchServiceClient client = new CustomerSearchServiceClient();
                Logger.Info(string.Concat("Search Request for ExportData:\n", SerializeObject<CustomerSearchRequest>(request)));

                response = client.ExportData(request);

                Logger.Info(string.Concat("Search Response for ExportData:\n", SerializeObject<CustomerSearchResponse>(response)));

                client.Close();
            }
            catch (Exception exception)
            {
                Logger.Error("Exception occurred at ExportData: \n" + exception);
            }

            return response;
        }

        /// <summary>
        /// Method that calls the start batch process within the CustomerSearchService
        /// </summary>
        /// <param name="request"></param>
        public void StartBatchProcess(CustomerSearchRequest request)
        {
            try
            {
                Logger.Info("Starting service connect for StartBatchProcess.");
                CustomerSearchServiceClient client = new CustomerSearchServiceClient();
                Logger.Info(string.Concat("Search Request for StartBatchProcess:\n", SerializeObject<CustomerSearchRequest>(request)));

                client.StartBatchProcess(request);
                client.Close();
            }
            catch (Exception exception)
            {
                Logger.Error("Exception occurred at StartBatchProcess: \n" + exception);
            }
        }

        #endregion
    }
}
