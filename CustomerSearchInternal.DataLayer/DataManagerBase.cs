﻿namespace CustomerSearchInternal.DataLayer
{
    using CustomerSearchInternal.DataLayer.CustomerSearchService;
    #region NAMESPACES
    using CustomerSearchInternal.Logging;
    using System;
    using System.Configuration;
    using System.Deployment.Application;
    using System.IO;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Xml.Serialization;
    #endregion

    public class DataManagerBase
    {
        #region PROPERTIES

        /// <summary>
        /// The Sql Server connection string
        /// </summary>
        public string SqlConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["SFGDB"].ConnectionString;
            }
        }

        /// <summary>
        /// Gets the current version number of the Customer Search application
        /// </summary>
        private static string CurrentVersion
        {
            get
            {
                return ApplicationDeployment.IsNetworkDeployed
                       ? ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString()
                       : Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize">Object to serialize.</param>
        /// <returns>A serialized string.</returns>
        protected string SerializeObject<T>(T toSerialize)
        {
            string serializedText = string.Empty;
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

                using (StringWriter textWriter = new StringWriter())
                {
                    xmlSerializer.Serialize(textWriter, toSerialize);
                    serializedText = textWriter.ToString();
                }
            }
            catch (Exception exception)
            {
                //// Log exception.
                Logger.Error(exception.ToString());
            }

            return serializedText;
        }

        /// <summary>
        /// Logs the XML text to log file.
        /// </summary>
        /// <param name="xmlText">The XML text.</param>
        /// <param name="requestType">Type of the request.</param>
        public void LogXML(string xmlText, string requestType)
        {
            if (!string.IsNullOrWhiteSpace(xmlText) && !string.IsNullOrWhiteSpace(requestType))
            {
                string lineEnd = "--------------------------------------------------------------------------------------------\n";

                Logger.Info(string.Format("\n----------------------------{0}-------------------------------\n", requestType) + Regex.Replace(xmlText, @"([>])\s*", "$1").Replace("><", ">\n<").Replace("> <", ">\n<") + "\n" + lineEnd);
            }
        }

        /// <summary>
        /// Create the source request used by the CustomerSearchService
        /// </summary>
        /// <param name="action"></param>
        /// <returns>Returns Source object</returns>
        public static Source CreateSourceRequest(string action)
        {
            return new Source
            {
                Action = action,
                ApplicationName = "Customer Search Internal Website",
                ApplicationVersion = CurrentVersion,
                //UserIdentity = DeluxeUser.CurrentUser.UserId
            };
        }
        #endregion Methods
    }
}
