﻿using CustomerSearchInternal.Common;
using CustomerSearchInternal.DataLayer.CustomerSearchService;
using CustomerSearchInternal.Interfaces;
using CustomerSearchInternal.Logging;
using CustomerSearchInternal.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerSearchInternal.DataLayer
{
    #region NAMESPACES

    #endregion
    public class ExportManager : DataManagerBase, IExportManager
    {
        /// <summary>
        /// Method used for exporting customer data
        /// </summary>
        /// <param name="customerList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="selectedRows"></param>
        /// <param name="IsExportMode"></param>
        /// <param name="CustomerDataOnly"></param>
        /// <param name="searchCriteria"></param>
        /// <param name="mainViewModel"></param>
        /// <returns>Returns DataTable</returns>
        public DataTable ExportData(string customerList, DateTime startDate, DateTime endDate, List<Models.CustomerDetail> selectedRows, bool IsExportMode, List<Models.SearchCriteria> searchCriteria, bool CustomerDataOnly) //Added CustomerDataOnly flag for ALM #2391 to export Customer details Without History.
        {
            DataTable dt = new DataTable();
            try
            {
                //Get the datatable with customer and order history data 
                dt = CreateExportFile(selectedRows, searchCriteria, IsExportMode, CustomerDataOnly, customerList);
            }
            catch (Exception exception)
            {
                Logger.Error("An error ocurred at ExportData()" + exception);
            }

            return dt;
        }


        /// <summary>
        /// Method to export the no match results
        /// </summary>
        /// <param name="noMatchCustomers"></param>
        /// <param name="isCustomerSearchMode"></param>
        /// <param name="importColumnCount"></param>
        /// <returns>Returns DataTable</returns>
        public DataTable ExportDataForNomatch(List<Models.SearchResult> noMatchCustomers)
        {
            DataTable dt = new DataTable();
            try
            {
                //Get the datatable for no match spreadsheet
                dt = CreateExportFileForNoMatch(noMatchCustomers);
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at ExportDataForNomatch()" + exception);
            }

            return dt;
        }

        /// <summary>
        /// Method to create the export file with or without history
        /// </summary>
        /// <param name="selectedRows"></param>
        /// <param name="searchCriteria"></param>
        /// <param name="IsExportMode"></param>
        /// <param name="customerDataOnly"></param>
        /// <param name="customerList"></param>
        /// <returns>Returns Datatable</returns>
        public DataTable CreateExportFile(List<Models.CustomerDetail> selectedRows, List<Models.SearchCriteria> searchCriteria, bool IsExportMode, bool customerDataOnly, string customerList)
        {
            CustomerSearchDataManager customerSearchDataManager = new CustomerSearchDataManager();
            DataTable dt = new DataTable();
            try
            {
                dt = ExcelExtensions.GetDataTableStructureForExport(customerDataOnly, false);

                if (!customerDataOnly)
                {
                    foreach (Models.CustomerDetail row in selectedRows)
                    {
                        //Get the order history for export
                        string bpcsCustNum = row.BPCSCustomerNumber;
                        DateTime startDate = DateTime.Now.AddYears(-3);
                        DateTime endDate = DateTime.Now;

                        row.OrderHistory = customerSearchDataManager.GetOrderHistory(bpcsCustNum, startDate, endDate);

                        foreach (Models.OrderHistory orderHistory in row.OrderHistory)
                        {
                            //Get the order details for each order history record
                            orderHistory.OrderDetails = customerSearchDataManager.GetOrderDetail(bpcsCustNum, orderHistory.InvoiceNumber, startDate, endDate);

                            foreach(Models.OrderDetail orderDetail in orderHistory.OrderDetails)
                            {
                                DataRow newRow = dt.NewRow();
                                newRow["Search Name"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Name) ? row.SearchCriteria.Name.ToString() : string.Empty;
                                newRow["Search Address"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Address) ? row.SearchCriteria.Address.ToString() : string.Empty;
                                newRow["Search City"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.City) ? row.SearchCriteria.City.ToString() : string.Empty;
                                newRow["Search State"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.State) ? row.SearchCriteria.State.ToString() : string.Empty;
                                newRow["Search Zip"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Zip) ? row.SearchCriteria.Zip.ToString() : string.Empty;
                                newRow["Search Phone"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Phone) ? row.SearchCriteria.Phone.ToString() : string.Empty;
                                newRow["Search Dist. ID"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.DistributorId) ? row.SearchCriteria.DistributorId.ToString() : string.Empty;
                                newRow["Customer Number"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.CustomerId) ? row.SearchCriteria.CustomerId.ToString() : string.Empty;
                                newRow["External Customer Number"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.ExternalCustomerId) ? row.SearchCriteria.ExternalCustomerId.ToString() : string.Empty;
                                newRow["Order History Date Range From"] = !string.IsNullOrWhiteSpace(row.StartDate.ToString()) ? row.StartDate.ToString() : string.Empty;
                                newRow["Order History Date Range To"] = !string.IsNullOrWhiteSpace(row.EndDate.ToString()) ? row.EndDate.ToString() : string.Empty;
                                newRow["Standardized Address Line"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.StandardizedAddress) ? row.SearchCriteria.StandardizedAddress.ToString() : string.Empty;
                                newRow["Standardized Zip"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.StandardizedZip) ? row.SearchCriteria.StandardizedZip.ToString() : string.Empty;
                                newRow["Permail Error Indicator"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.PerMailError) ? row.SearchCriteria.PerMailError.ToString() : string.Empty;
                                newRow["Reference Number"] = !string.IsNullOrWhiteSpace(row.ReferenceNumber) ? row.ReferenceNumber.ToString() : string.Empty;
                                newRow["Distributor ID"] = !string.IsNullOrWhiteSpace(row.DistributorID) ? row.DistributorID.ToString() : string.Empty;
                                newRow["Distributor Name"] = !string.IsNullOrWhiteSpace(row.DistributorName) ? row.DistributorName.ToString() : string.Empty;
                                newRow["Distributor Phone#"] = !string.IsNullOrWhiteSpace(row.DistributorPhoneNumber) ? row.DistributorPhoneNumber.ToString() : string.Empty;
                                newRow["Distributor Fax#"] = !string.IsNullOrWhiteSpace(row.DistributorFaxNumber) ? row.DistributorFaxNumber.ToString() : string.Empty;
                                newRow["Distributor Email Address"] = !string.IsNullOrWhiteSpace(row.DistributorEmailAddress) ? row.DistributorEmailAddress.ToString() : string.Empty;
                                newRow["Last Invoice Date"] = !string.IsNullOrWhiteSpace(row.LastInvoiceDateValue) ? row.LastInvoiceDateValue.ToString() : string.Empty;
                                newRow["First Order Date"] = !string.IsNullOrWhiteSpace(row.FirstOrderDateValue) ? row.FirstOrderDateValue.ToString() : string.Empty;
                                newRow["Last Order Date"] = !string.IsNullOrWhiteSpace(row.LastOrderDateValue) ? row.LastOrderDateValue.ToString() : string.Empty;
                                newRow["Customer Status"] = !string.IsNullOrWhiteSpace(row.CustomerStatus) ? row.CustomerStatus.ToString() : string.Empty;
                                newRow["BPCS Customer #"] = !string.IsNullOrWhiteSpace(row.BPCSCustomerNumber) ? row.BPCSCustomerNumber.ToString() : string.Empty;
                                newRow["Ext. Customer #"] = !string.IsNullOrWhiteSpace(row.ExternalCustomerNumber) ? row.ExternalCustomerNumber.ToString() : string.Empty;
                                newRow["Bill to Name"] = !string.IsNullOrWhiteSpace(row.BillToName) ? row.BillToName.ToString() : string.Empty;
                                newRow["Bill to Addr1"] = !string.IsNullOrWhiteSpace(row.BillToAddress1) ? row.BillToAddress1.ToString() : string.Empty;
                                newRow["Bill to Addr2"] = !string.IsNullOrWhiteSpace(row.BillToAddress2) ? row.BillToAddress2.ToString() : string.Empty;
                                newRow["Bill to City"] = !string.IsNullOrWhiteSpace(row.BillToCity) ? row.BillToCity.ToString() : string.Empty;
                                newRow["Bill to State"] = !string.IsNullOrWhiteSpace(row.BillToState) ? row.BillToState.ToString() : string.Empty;
                                newRow["Bill to Zip"] = !string.IsNullOrWhiteSpace(row.BillToZip) ? row.BillToZip.ToString() : string.Empty;
                                newRow["Bill to Phone"] = !string.IsNullOrWhiteSpace(row.BillToPhone) ? row.BillToPhone.ToString() : string.Empty;
                                newRow["Cust Ship to Name"] = !string.IsNullOrWhiteSpace(row.ShipToName) ? row.ShipToName.ToString() : string.Empty;
                                newRow["Cust Ship to Addr1"] = !string.IsNullOrWhiteSpace(row.ShipToAddress1) ? row.ShipToAddress1.ToString() : string.Empty;
                                newRow["Cust Ship to Addr2"] = !string.IsNullOrWhiteSpace(row.ShipToAddress2) ? row.ShipToAddress2.ToString() : string.Empty;
                                newRow["Cust Ship to City"] = !string.IsNullOrWhiteSpace(row.ShipToCity) ? row.ShipToCity.ToString() : string.Empty;
                                newRow["Cust Ship to State"] = !string.IsNullOrWhiteSpace(row.ShipToState) ? row.ShipToState.ToString() : string.Empty;
                                newRow["Cust Ship to Zip"] = !string.IsNullOrWhiteSpace(row.ShipToZip) ? row.ShipToZip.ToString() : string.Empty;
                                newRow["Name Match"] = !string.IsNullOrWhiteSpace(row.NameMatch) ? row.NameMatch.ToString() : string.Empty;
                                newRow["Addr Match"] = !string.IsNullOrWhiteSpace(row.AddressMatch) ? row.AddressMatch.ToString() : string.Empty;
                                newRow["Phone Match"] = !string.IsNullOrWhiteSpace(row.PhoneMatch) ? row.PhoneMatch.ToString() : string.Empty;
                                newRow["Rotation Exception"] = !string.IsNullOrWhiteSpace(row.DoNotRotate) ? row.DoNotRotate.ToString() : string.Empty;

                                //Rows specifically for order history
                                newRow["Order Date"] = !string.IsNullOrWhiteSpace(orderHistory.OrderDateValue) ? orderHistory.OrderDateValue.ToString() : string.Empty;
                                newRow["Order #"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Ext Order #"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Distributor ID"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["PO #"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Invoice #"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Invoice Date"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Bill to Name"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Bill to Address 1"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Bill to Address 2"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Bill to City"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Bill to State"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Bill to Zip"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Ship to Name"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Ship to Address 1"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Ship to Address 2"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Ship to City"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Ship to State"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Order Ship to Zip"] = !string.IsNullOrWhiteSpace(orderDetail.OrderNumber) ? orderDetail.OrderNumber.ToString() : string.Empty;
                                newRow["Line #"] = !string.IsNullOrWhiteSpace(orderDetail.LineItemNumber) ? orderDetail.LineItemNumber.ToString() : string.Empty;
                                newRow["Transaction Type"] = !string.IsNullOrWhiteSpace(orderDetail.TransactionType) ? orderDetail.TransactionType.ToString() : string.Empty;
                                newRow["Prod Line"] = !string.IsNullOrWhiteSpace(orderDetail.ProductLine) ? orderDetail.ProductLine.ToString() : string.Empty;
                                newRow["Prod Code"] = !string.IsNullOrWhiteSpace(orderDetail.ProductCode) ? orderDetail.ProductCode.ToString() : string.Empty;
                                newRow["Prod Description"] = !string.IsNullOrWhiteSpace(orderDetail.ProductDescription) ? orderDetail.ProductDescription.ToString() : string.Empty;
                                newRow["Quantity"] = !string.IsNullOrWhiteSpace(orderDetail.Quantity) ? orderDetail.Quantity.ToString() : string.Empty;
                                newRow["Invoice Amount"] = !string.IsNullOrWhiteSpace(orderDetail.InvoiceTotal) ? orderDetail.InvoiceTotal.ToString() : string.Empty;
                                newRow["Forms Amount"] = !string.IsNullOrWhiteSpace(orderDetail.FormsAmount.ToString()) ? orderDetail.FormsAmount.ToString() : string.Empty;
                                newRow["Commission Amount"] = !string.IsNullOrWhiteSpace(orderDetail.CommissionAmount.ToString()) ? orderDetail.CommissionAmount.ToString() : string.Empty;
                                newRow["Margin Amount"] = !string.IsNullOrWhiteSpace(orderDetail.MarginAmount.ToString()) ? orderDetail.FreightAmount.ToString() : string.Empty;
                                newRow["Freight Amount"] = !string.IsNullOrWhiteSpace(orderDetail.FreightAmount.ToString()) ? orderDetail.LineItemNumber.ToString() : string.Empty;
                                newRow["Vendor ID"] = !string.IsNullOrWhiteSpace(orderDetail.VendorID) ? orderDetail.VendorID.ToString() : string.Empty;
                                newRow["Vendor Name"] = !string.IsNullOrWhiteSpace(orderDetail.VendorName) ? orderDetail.VendorName.ToString() : string.Empty;
                                newRow["2014 Non-Integrated BAM Order History - FINANCIALS NOT VALIDATED"] = string.Empty;

                                dt.Rows.Add(newRow);
                            }
                        }
                    }
                }
                else
                {
                    foreach (Models.CustomerDetail row in selectedRows)
                    {
                        DataRow newRow = dt.NewRow();
                        newRow["Search Name"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Name) ? row.SearchCriteria.Name.ToString() : string.Empty;
                        newRow["Search Address"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Address) ? row.SearchCriteria.Address.ToString() : string.Empty;
                        newRow["Search City"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.City) ? row.SearchCriteria.City.ToString() : string.Empty;
                        newRow["Search State"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.State) ? row.SearchCriteria.State.ToString() : string.Empty;
                        newRow["Search Zip"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Zip) ? row.SearchCriteria.Zip.ToString() : string.Empty;
                        newRow["Search Phone"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.Phone) ? row.SearchCriteria.Phone.ToString() : string.Empty;
                        newRow["Search Dist. ID"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.DistributorId) ? row.SearchCriteria.DistributorId.ToString() : string.Empty;
                        newRow["Customer Number"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.CustomerId) ? row.SearchCriteria.CustomerId.ToString() : string.Empty;
                        newRow["External Customer Number"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.ExternalCustomerId) ? row.SearchCriteria.ExternalCustomerId.ToString() : string.Empty;
                        newRow["Order History Date Range From"] = !string.IsNullOrWhiteSpace(row.StartDate.ToString()) ? row.StartDate.ToString() : string.Empty;
                        newRow["Order History Date Range To"] = !string.IsNullOrWhiteSpace(row.EndDate.ToString()) ? row.EndDate.ToString() : string.Empty;
                        newRow["Standardized Address Line"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.StandardizedAddress) ? row.SearchCriteria.StandardizedAddress.ToString() : string.Empty;
                        newRow["Standardized Zip"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.StandardizedZip) ? row.SearchCriteria.StandardizedZip.ToString() : string.Empty;
                        newRow["Permail Error Indicator"] = !string.IsNullOrWhiteSpace(row.SearchCriteria.PerMailError) ? row.SearchCriteria.PerMailError.ToString() : string.Empty;
                        newRow["Reference Number"] = !string.IsNullOrWhiteSpace(row.ReferenceNumber) ? row.ReferenceNumber.ToString() : string.Empty;
                        newRow["Distributor ID"] = !string.IsNullOrWhiteSpace(row.DistributorID) ? row.DistributorID.ToString() : string.Empty;
                        newRow["Distributor Name"] = !string.IsNullOrWhiteSpace(row.DistributorName) ? row.DistributorName.ToString() : string.Empty;
                        newRow["Distributor Phone#"] = !string.IsNullOrWhiteSpace(row.DistributorPhoneNumber) ? row.DistributorPhoneNumber.ToString() : string.Empty;
                        newRow["Distributor Fax#"] = !string.IsNullOrWhiteSpace(row.DistributorFaxNumber) ? row.DistributorFaxNumber.ToString() : string.Empty;
                        newRow["Distributor Email Address"] = !string.IsNullOrWhiteSpace(row.DistributorEmailAddress) ? row.DistributorEmailAddress.ToString() : string.Empty;
                        newRow["Last Invoice Date"] = !string.IsNullOrWhiteSpace(row.LastInvoiceDateValue) ? row.LastInvoiceDateValue.ToString() : string.Empty;
                        newRow["First Order Date"] = !string.IsNullOrWhiteSpace(row.FirstOrderDateValue) ? row.FirstOrderDateValue.ToString() : string.Empty;
                        newRow["Last Order Date"] = !string.IsNullOrWhiteSpace(row.LastOrderDateValue) ? row.LastOrderDateValue.ToString() : string.Empty;
                        newRow["Customer Status"] = !string.IsNullOrWhiteSpace(row.CustomerStatus) ? row.CustomerStatus.ToString() : string.Empty;
                        newRow["BPCS Customer #"] = !string.IsNullOrWhiteSpace(row.BPCSCustomerNumber) ? row.BPCSCustomerNumber.ToString() : string.Empty;
                        newRow["Ext. Customer #"] = !string.IsNullOrWhiteSpace(row.ExternalCustomerNumber) ? row.ExternalCustomerNumber.ToString() : string.Empty;
                        newRow["Bill to Name"] = !string.IsNullOrWhiteSpace(row.BillToName) ? row.BillToName.ToString() : string.Empty;
                        newRow["Bill to Addr1"] = !string.IsNullOrWhiteSpace(row.BillToAddress1) ? row.BillToAddress1.ToString() : string.Empty;
                        newRow["Bill to Addr2"] = !string.IsNullOrWhiteSpace(row.BillToAddress2) ? row.BillToAddress2.ToString() : string.Empty;
                        newRow["Bill to City"] = !string.IsNullOrWhiteSpace(row.BillToCity) ? row.BillToCity.ToString() : string.Empty;
                        newRow["Bill to State"] = !string.IsNullOrWhiteSpace(row.BillToState) ? row.BillToState.ToString() : string.Empty;
                        newRow["Bill to Zip"] = !string.IsNullOrWhiteSpace(row.BillToZip) ? row.BillToZip.ToString() : string.Empty;
                        newRow["Bill to Phone"] = !string.IsNullOrWhiteSpace(row.BillToPhone) ? row.BillToPhone.ToString() : string.Empty;
                        newRow["Cust Ship to Name"] = !string.IsNullOrWhiteSpace(row.ShipToName) ? row.ShipToName.ToString() : string.Empty;
                        newRow["Cust Ship to Addr1"] = !string.IsNullOrWhiteSpace(row.ShipToAddress1) ? row.ShipToAddress1.ToString() : string.Empty;
                        newRow["Cust Ship to Addr2"] = !string.IsNullOrWhiteSpace(row.ShipToAddress2) ? row.ShipToAddress2.ToString() : string.Empty;
                        newRow["Cust Ship to City"] = !string.IsNullOrWhiteSpace(row.ShipToCity) ? row.ShipToCity.ToString() : string.Empty;
                        newRow["Cust Ship to State"] = !string.IsNullOrWhiteSpace(row.ShipToState) ? row.ShipToState.ToString() : string.Empty;
                        newRow["Cust Ship to Zip"] = !string.IsNullOrWhiteSpace(row.ShipToZip) ? row.ShipToZip.ToString() : string.Empty;
                        newRow["Name Match"] = !string.IsNullOrWhiteSpace(row.NameMatch) ? row.NameMatch.ToString() : string.Empty;
                        newRow["Addr Match"] = !string.IsNullOrWhiteSpace(row.AddressMatch) ? row.AddressMatch.ToString() : string.Empty;
                        newRow["Phone Match"] = !string.IsNullOrWhiteSpace(row.PhoneMatch) ? row.PhoneMatch.ToString() : string.Empty;
                        newRow["Rotation Exception"] = !string.IsNullOrWhiteSpace(row.DoNotRotate) ? row.DoNotRotate.ToString() : string.Empty;

                        dt.Rows.Add(newRow);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
            }
            finally
            {
                //// Release resources.
                //if (excelApplication != null)
                //{
                //    excelApplication.Quit();
                //    //Marshal.FinalReleaseComObject(excelApplication);
                //}
            }

            return dt;
        }

        /// <summary>
        /// Method to create the export file for no matches
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns>Returns Datatable</returns>
        public DataTable CreateExportFileForNoMatch(List<Models.SearchResult> searchCriteria)
        {
            CustomerSearchDataManager customerSearchDataManager = new CustomerSearchDataManager();
            DataTable dt = new DataTable();
            try
            {
                dt = ExcelExtensions.GetDataTableStructureForExport(true, true);

                foreach (Models.SearchResult criteria in searchCriteria)
                {
                    DataRow newRow = dt.NewRow();
                    newRow["Reference Number"] = !string.IsNullOrWhiteSpace(criteria.Criteria.ReferenceNumber) ? criteria.Criteria.ReferenceNumber.ToString() : string.Empty;
                    newRow["Customer Number"] = !string.IsNullOrWhiteSpace(criteria.Criteria.CustomerId) ? criteria.Criteria.CustomerId.ToString() : string.Empty;
                    newRow["External Customer Number"] = !string.IsNullOrWhiteSpace(criteria.Criteria.ExternalCustomerId) ? criteria.Criteria.ExternalCustomerId.ToString() : string.Empty;
                    newRow["No Match (Y/N)"] = "Y";
                    newRow["Error (Y/N)"] = !string.IsNullOrWhiteSpace(criteria.Criteria.ErrorMatch) ? criteria.Criteria.ErrorMatch.ToString() : string.Empty;

                    dt.Rows.Add(newRow);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.ToString());
            }

            return dt;
        }


        /// <summary>
        /// Method that calls the CustomerSearchService to start the batch process for large spreadsheets
        /// </summary>
        public void StartBatchProcess()
        {
            try
            {
                CustomerSearchRequest request = new CustomerSearchRequest
                {
                    Source = CreateSourceRequest("StartBatchProcess")
                };
                CustomerSearchServiceManager customerSearchServiceManager = new CustomerSearchServiceManager();
                customerSearchServiceManager.StartBatchProcess(request);
            }
            catch (Exception exception)
            {
                Logger.Error("Error occurred at StartBatchProcess() " + exception);
            }
        }

        #region PRIVATE METHODS
        /// <summary>
        /// Create the CustomerSearchRequest object
        /// </summary>
        /// <param name="customerList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="selectedRows"></param>
        /// <param name="isExportMode"></param>
        /// <param name="searchCriteria"></param>
        /// <param name="customerDataOnly"></param>
        /// <param name="fileName">todo: describe fileName parameter on CreateCustomerSearchRequest</param>
        /// <param name="exportType">todo: describe exportType parameter on CreateCustomerSearchRequest</param>
        /// <returns>Returns CustomerSearchRequest object</returns>
        private static CustomerSearchRequest CreateCustomerSearchRequest(string customerList, DateTime startDate, DateTime endDate, List<Models.CustomerDetail> selectedRows, bool isExportMode, List<Models.SearchCriteria> searchCriteria, string fileName, bool customerDataOnly, ExportType exportType)
        {
            CustomerSearchRequest request = new CustomerSearchRequest
            {
                Export = new Export
                {
                    FileName = fileName,
                    ExportType = exportType
                },
                CustomerSearch = new CustomerSearch
                {
                    CustomerList = customerList,
                    StartDate = startDate,
                    EndDate = endDate,
                    IsExportMode = isExportMode,
                    IsCustomerDataOnly = customerDataOnly,
                    Customers = selectedRows.ConvertAll(x => new CustomerSearchService.CustomerDetail
                    {
                        AddressMatch = x.AddressMatch,
                        BillToAddress1 = x.BillToAddress1,
                        BillToAddress2 = x.BillToAddress2,
                        BillToCity = x.BillToCity,
                        BillToName = x.BillToName,
                        BillToPhone = x.BillToPhone,
                        BillToState = x.BillToState,
                        BillToZip = x.BillToZip,
                        BPCSCustomerNumber = x.BPCSCustomerNumber,
                        CustomerStatus = x.CustomerStatus,
                        DistributorEmailAddress = x.DistributorEmailAddress,
                        DistributorFaxNumber = x.DistributorFaxNumber,
                        DistributorID = x.DistributorID,
                        DistributorName = x.DistributorName,
                        DistributorPhoneNumber = x.DistributorPhoneNumber,
                        DoNotRotate = x.DoNotRotate,
                        EndDate = x.EndDate,
                        ExternalCustomerNumber = x.ExternalCustomerNumber,
                        FirstOrderDate = x.FirstOrderDate,
                        IsChecked = x.IsChecked,
                        LastInvoiceDate = x.LastInvoiceDate,
                        LastOrderDate = x.LastOrderDate,
                        NameMatch = x.NameMatch,
                        PhoneMatch = x.PhoneMatch,
                        ReferenceNumber = x.ReferenceNumber,
                        ScannedItem = x.ScannedItem,
                        ShipToAddress1 = x.ShipToAddress1,
                        ShipToAddress2 = x.ShipToAddress2,
                        ShipToCity = x.ShipToCity,
                        ShipToName = x.ShipToName,
                        ShipToState = x.ShipToState,
                        ShipToZip = x.ShipToZip,
                        StartDate = x.StartDate,
                        OrderHistoryDetails = x.OrderHistory.ToList().ConvertAll(y => new CustomerSearchService.OrderHistory
                        {
                            BPCSCustomerNumber = y.BPCSCustomerNumber,
                            EndDate = y.EndDate,
                            ExternalOrderNumber = y.ExternalOrderNumber,
                            InvoiceDate = y.InvoiceDate,
                            InvoiceNumber = y.InvoiceNumber,
                            InvoiceTotal = y.InvoiceTotal,
                            IsHistoryData = y.IsHistoryData,
                            OrderDate = y.OrderDate,
                            OrderDistributorId = y.OrderDistributorId,
                            OrderNumber = y.OrderNumber,
                            PromisedOrderNumber = y.PromisedOrderNumber,
                            ScannedItem = y.ScannedItem,
                            ShipToAddress1 = y.ShipToAddress1,
                            ShipToAddress2 = y.ShipToAddress2,
                            ShipToCity = y.ShipToCity,
                            ShipToName = y.ShipToName,
                            ShipToState = y.ShipToState,
                            ShipToZip = y.ShipToZip,
                            StartDate = y.StartDate,
                            TotalCommissionAmount = y.TotalCommissionAmount,
                            TotalFormsAmount = y.TotalFormsAmount,
                            TotalFreightAmount = y.TotalFreightAmount,
                            TotalMarginAmount = y.TotalMarginAmount,
                            TwelveMonthsTotal = y.TwelveMonthsTotal,
                            OrderDetails = y.OrderDetails.ToList().ConvertAll(z => new CustomerSearchService.OrderDetail
                            {
                                BPCSCustomerNumber = z.BPCSCustomerNumber,
                                CommissionAmount = z.CommissionAmount,
                                EndDate = z.EndDate,
                                FormsAmount = z.FormsAmount,
                                FreightAmount = z.FreightAmount,
                                InvoiceTotal = z.InvoiceTotal,
                                IsHistoryData = z.IsHistoryData,
                                LineItemNumber = z.LineItemNumber,
                                MarginAmount = z.MarginAmount,
                                OrderNumber = z.OrderNumber,
                                ProductCode = z.ProductCode,
                                ProductDescription = z.ProductDescription,
                                ProductLine = z.ProductLine,
                                PurchaseOrderNumber = z.PurchaseOrderNumber,
                                Quantity = z.Quantity,
                                QuantityPerProductCode = z.QuantityPerProductCode,
                                ScannedItem = z.ScannedItem,
                                ShipToAddress = z.ShipToAddress,
                                ShipToCity = z.ShipToCity,
                                ShipToName = z.ShipToName,
                                ShipToState = z.ShipToState,
                                ShipToZip = z.ShipToZip,
                                StartDate = z.StartDate,
                                TransactionType = z.TransactionType,
                                VendorID = z.VendorID,
                                VendorName = z.VendorName
                            }).ToArray(),
                        }).ToArray()
                    }).ToArray(),
                    SearchCriterias = searchCriteria.ConvertAll(x => new CustomerSearchService.SearchCriteria
                    {
                        Address1 = x.Address1,
                        Address2 = x.Address2,
                        BPCSCustomerNumber = x.BPCSCustomerNumber,
                        CriteriaString = x.CriteriaString,
                        CustomerNumber = x.CustomerId,
                        DistributorAddress = x.Address,
                        DistributorCity = x.City,
                        DistributorId = x.DistributorId,
                        DistributorIdForExport = x.DistributorIdForExport,
                        DistributorName = x.Name,
                        DistributorPhone = x.Phone,
                        DistributorState = x.State,
                        DistributorZip = x.Zip,
                        EndDate = x.EndDate,
                        ErrorMatch = x.ErrorMatch,
                        ExternalCustomerNumber = x.ExternalCustomerId,
                        PerMailError = x.PerMailError,
                        ReferenceNumber = x.ReferenceNumber,
                        ScannedItem = x.ScannedItem,
                        StandardizedAddress = x.StandardizedAddress,
                        StandardizedZip = x.StandardizedZip,
                        StartDate = x.StartDate
                    }).ToArray()
                }
            };

            return request;
        }

        #endregion
    }
}
