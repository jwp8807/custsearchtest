﻿namespace CustomerSearchInternal.Logging
{
    #region NAMESPACES
    using log4net;
    #endregion

    public static class Logger
    {
        #region FIELDS
        private static ILog logManager = LogManager.GetLogger(typeof(Logger));
        #endregion

        #region PUBLIC METHODS
        /// <summary>  
        /// Used to log Debug messages in an explicit Debug Logger  
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        public static void Debug(string message)
        {
            logManager.Debug(message);
        }


        /// <summary>  
        ///  
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        /// <param name="exception">The exception to log, including its stack trace </param>  
        public static void Debug(string message, System.Exception exception)
        {
            logManager.Debug(message, exception);
        }


        /// <summary>  
        ///  The info log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        public static void Info(string message)
        {
            logManager.Info(message);
        }


        /// <summary>  
        ///  The info log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        /// <param name="exception">The exception to log, including its stack trace </param>  
        public static void Info(string message, System.Exception exception)
        {
            logManager.Info(message, exception);
        }

        /// <summary>  
        ///  The warning log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        public static void Warn(string message)
        {
            logManager.Warn(message);
        }

        /// <summary>  
        ///  The warning log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        /// <param name="exception">The exception to log, including its stack trace </param>  
        public static void Warn(string message, System.Exception exception)
        {
            logManager.Warn(message, exception);
        }

        /// <summary>  
        ///  The error log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        public static void Error(string message)
        {
            logManager.Error(message);
        }

        /// <summary>  
        ///  The error log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        /// <param name="exception">The exception to log, including its stack trace </param>  
        public static void Error(string message, System.Exception exception)
        {
            logManager.Error(message, exception);
        }


        /// <summary>  
        ///  The fatal log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        public static void Fatal(string message)
        {
            logManager.Fatal(message);
        }

        /// <summary>  
        ///  The fatal log method
        /// </summary>  
        /// <param name="message">The object message to log</param>  
        /// <param name="exception">The exception to log, including its stack trace </param>  
        public static void Fatal(string message, System.Exception exception)
        {
            logManager.Fatal(message, exception);
        }

        #endregion
    }
}
