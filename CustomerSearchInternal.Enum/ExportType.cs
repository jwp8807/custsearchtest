﻿namespace CustomerSearchInternal.Enum
{
    public enum ExportType
    {
        WithHistory,
        WithoutHistory,
        NoMatch
    }
}
