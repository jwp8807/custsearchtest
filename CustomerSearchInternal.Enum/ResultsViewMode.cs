﻿namespace CustomerSearchInternal.Enum
{
    public enum ResultsViewMode
    {
        Compact,
        Stretch
    }
}
