﻿namespace CustomerSearchInternal.Enum
{
    /// <summary>
    /// The message type enums
    /// </summary>
    public enum MessageType
    {
        Information,
        Error,
        DataAdded
    }
}
